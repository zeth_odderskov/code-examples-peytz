# Action Map-modul

**Kunde:**  Nordic Safe Cities  
**Live-eksempel:**  [https://nordicsafecities.org/publications/chapter-1-safe-city-blueprint/#acf-block-2](https://nordicsafecities.org/publications/chapter-1-safe-city-blueprint/#acf-block-2)

## Kort om opgaven

Her var der et grafisk bureau der havde udtænkt et meget custom modul, som vi skulle have til at fungere bedst på desktop, men også så godt som muligt på telefoner. 

Kunden var vildt tændt på ideen, så vi skulle få det lavet så godt som muligt - uden at det skulle forkromes.

## Hvad demonstrerer denne kode

Dette er et custom Vue-modul der er bygget ovenpå en masse ACF-felter (custom fields). 

Det demonstrerer hvordan jeg har løst dette specielle modul og bygget det på et WordPress-site. 


## Tidsforbrug

**18 timer**

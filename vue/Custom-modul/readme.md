# Custom modul

**Kunde:**  JV Consulting  
**Live-eksempel:**  [https://jvconsulting.dk/](https://jvconsulting.dk/)

## Kort om opgaven

Dette var en lille hurtig hjemmeside for en ny-startet selvstændig. 
Så vi skulle finde en god balance mellem 'fit-n-finish' og pris (som oftest favoriserede prisen).

Dette modul er et eksempel på, hvor jeg har løst et custom designet modul på diverse forskellige enhedsstørrelser.

## Hvad demonstrerer denne kode

Her kan man se et mere almindeligt Vue-modul, på et custom-designet og alligevel almindeligt modul.

Udfordringen her var at løse det, så koden var let at læse og vedligeholde, men hvor at der ikke var nogen skærmstørrelser, hvor modulet så skørt ud. 


## Tidsforbrug

**8 timer**

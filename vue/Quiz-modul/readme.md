# Quiz modul

**Kunde:** Danske Taler  
**Live-eksempel:**  [https://dansketaler.dk/quiz/kvindequizzen/](https://dansketaler.dk/quiz/kvindequizzen/)

## Kort om opgaven

Dette var et ønske om at kunne lave quizzer selv via WordPress-backenden. 
Det er bygget ovenpå Advanced Custom Fields, som tilføjer denne 'repeater'-mulighed, så kunden selv kan lave så mange slides og så mange svar-muligheder som de ønsker.

## Hvad demonstrerer denne kode

Denne kode demonstrerer et eksempel på at bygge noget custom ind i WordPress, som WordPress på overfladen slet ikke er lavet eller gearet til. Og hvordan det i praksis faktisk fungerer godt. 

Derudover kan man se min kodestil i større Vue-komponenter.



## Tidsforbrug

**15 timer**

Det tog dog så kort tid, da jeg havde lavet modulet tidligere til ['Er du for Sød'](https://erduforsoed.dk/quiz-hvor-soed-er-danmark/). Dengang havde det taget omkring 30-40 timer at lave (inklusiv projektstyring).

# ER-diagram

**Kunde:** Athleeto  
**Live-eksempel:** Dette projekt er fortsat under udarbejdelse

## Kort om opgaven

Her skulle jeg stille en API til rådighed, der gemte data i en database. API'et skulle bruges af en Xamarin-app.

## Hvad demonstrerer denne kode

Dette diagram er for at vise opbyggelsen af en mere kompleks database og hvor langt mine backend-kompetencer rækker. Jeg har designet strukturen der er vist og har efterfølgende lavet migrations i Laravel der afspejler diagrammet.

Der er mange steder at dette kan forbedres.

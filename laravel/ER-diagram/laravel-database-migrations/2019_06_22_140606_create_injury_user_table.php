<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInjuryUserTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up(){
		Schema::create( 'injury_user', function( Blueprint $table ){
			$table->bigIncrements( 'id' );
			$table->timestamps();
			$table->unsignedBigInteger( 'user_id' );
			$table->unsignedBigInteger( 'injury_id' );
			$table->dateTime( 'injury_start' );
			$table->text('description' )->nullable();
			$table->dateTime( 'cured' )->nullable();
			$table->softDeletes();
		} );
		Schema::table( 'injury_user', function( Blueprint $table ){
			$table->foreign( 'user_id' )->references( 'id' )->on( 'users' );
			$table->foreign( 'injury_id' )->references( 'id' )->on( 'injuries' );
		});
	}





	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down(){
		Schema::dropIfExists( 'injury_user' );
	}
}

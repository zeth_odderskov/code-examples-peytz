<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecurringQuestionUserTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up(){
		Schema::create( 'recurring_question_user', function( Blueprint $table ){
			$table->id();
			$table->timestamps();
			$table->unsignedBigInteger( 'user_id' );
			$table->dateTime( 'due' );
			$table->unsignedBigInteger( 'recurring_question_id' );
			$table->text( 'answer' );
			$table->text( 'coach_comment' );
			$table->softDeletes();
		} );

		Schema::table( 'recurring_question_user', function( Blueprint $table ){
			$table->foreign( 'user_id' )->references( 'id' )->on( 'users' );
			$table->foreign( 'recurring_question_id' )->references( 'id' )->on( 'recurring_questions' );
		});
	}





	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down(){
		Schema::dropIfExists( 'recurring_question_user' );
	}
}

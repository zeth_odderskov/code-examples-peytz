<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInjuriesTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up(){
		Schema::create( 'injuries', function( Blueprint $table ){
			$table->bigIncrements( 'id' );
			$table->timestamps();
			$table->string( 'title' );
			$table->string( 'body_part' );
			$table->string( 'description' )->nullable();
			$table->string( 'answer_type' )->default( 'range' );
			$table->string( 'side' )->nullable();
			$table->unsignedBigInteger( 'lower_boundary' )->nullable();
			$table->unsignedBigInteger( 'upper_boundary' )->nullable();
			$table->unsignedBigInteger( 'parent_id' )->nullable()->default( null );
			$table->unsignedBigInteger( 'user_id' )->nullable();
			$table->unsignedBigInteger( 'team_id' )->nullable();
			$table->softDeletes();
		} );
		Schema::table( 'injuries', function( Blueprint $table ){
			$table->foreign( 'parent_id' )->references( 'id' )->on( 'injuries' );
			$table->foreign( 'user_id' )->references( 'id' )->on( 'users' );
			$table->foreign( 'team_id' )->references( 'id' )->on( 'teams' );
		});
	}





	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down(){
		Schema::dropIfExists( 'injuries' );
	}
}

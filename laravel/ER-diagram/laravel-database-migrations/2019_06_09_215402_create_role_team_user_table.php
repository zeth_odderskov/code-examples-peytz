<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoleTeamUserTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up(){
		Schema::create( 'role_team_user', function( Blueprint $table ){
//			$table->primary( [ 'user_id', 'team_id' ] );
			$table->bigIncrements( 'id' );
			$table->timestamps();
			$table->unsignedBigInteger( 'user_id' );
			$table->unsignedBigInteger( 'team_id' );
			$table->unsignedBigInteger( 'role_id' );
//			$table->dateTime( 'deleted' )->nullable();
			$table->softDeletes();
		} );
		Schema::table( 'role_team_user', function( Blueprint $table ){
			$table->foreign( 'user_id' )->references( 'id' )->on( 'users' );
			$table->foreign( 'team_id' )->references( 'id' )->on( 'teams' );
			$table->foreign( 'role_id' )->references( 'id' )->on( 'roles' );
		});
	}





	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down(){
		Schema::dropIfExists( 'role_team_user' );
	}
}

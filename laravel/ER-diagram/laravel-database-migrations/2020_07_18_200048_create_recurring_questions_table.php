<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecurringQuestionsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up(){
		Schema::create( 'recurring_questions', function( Blueprint $table ){
			$table->id();
			$table->timestamps();
			$table->unsignedBigInteger('user_id');
			$table->unsignedBigInteger( 'team_id' );
			$table->dateTime( 'activated_at' );
			$table->dateTime( 'deactivated_at' )->nullable();
			$table->unsignedBigInteger( 'question_id' );
			$table->string( 'frequency' );
			$table->softDeletes();
		} );

		Schema::table( 'recurring_questions', function (Blueprint $table){
			$table->foreign( 'question_id' )->references( 'id' )->on( 'questions' );
			$table->foreign( 'user_id' )->references( 'id' )->on( 'users' );
			$table->foreign( 'team_id' )->references( 'id' )->on( 'teams' );
		} );
	}





	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down(){
		Schema::dropIfExists( 'recurring_questions' );
	}
}

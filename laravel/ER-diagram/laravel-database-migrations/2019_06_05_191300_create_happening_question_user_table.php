<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHappeningQuestionUserTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up(){
		Schema::create( 'happening_question_user', function( Blueprint $table ){
			$table->bigIncrements( 'id' );
			$table->timestamps();
			$table->unsignedBigInteger('user_id' );
			$table->unsignedBigInteger('happening_id' );
			$table->unsignedBigInteger('question_id' );
			$table->text('answer_content' )->nullable();
			$table->text('coach_comment' )->nullable();
			$table->text('team_comment' )->nullable();
			$table->bigInteger('duration' )->nullable();
			$table->softDeletes();
		} );
		Schema::table( 'happening_question_user', function( Blueprint $table ){
			$table->foreign( 'user_id' )->references( 'id' )->on( 'users' );
			$table->foreign( 'happening_id' )->references( 'id' )->on( 'happenings' );
			$table->foreign( 'question_id' )->references( 'id' )->on( 'questions' );
			// The foreign-key to the answers-table is made, after the answers-table is created.
		});
	}





	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down(){
		Schema::dropIfExists( 'happening_question_user_team' );
	}
}

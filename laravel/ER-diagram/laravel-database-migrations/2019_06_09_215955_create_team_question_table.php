<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamQuestionTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up(){
		Schema::create( 'team_question', function( Blueprint $table ){
//			$table->primary( [ 'team_id', 'question_id'] );
			$table->bigIncrements( 'id' );
			$table->timestamps();
			$table->unsignedBigInteger( 'team_id' )->nullable();
			$table->unsignedBigInteger( 'question_id' );
			$table->unsignedBigInteger( 'asked_upon_happening_type_id' );
			$table->string( 'asked_when_happening_is' )->default( 'finished' );
			$table->dateTime( 'activated_at' )->default( \Carbon\Carbon::now() );
			$table->dateTime( 'deactivated_at' )->nullable();
			$table->boolean( 'default' )->default( false );
			$table->softDeletes();
		} );

		Schema::table( 'team_question', function( Blueprint $table ){
			$table->foreign( 'team_id' )->references( 'id' )->on( 'teams' );
			$table->foreign( 'question_id' )->references( 'id' )->on( 'questions' );
			$table->foreign( 'asked_upon_happening_type_id' )->references( 'id' )->on( 'happening_types' );
		});
	}





	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down(){
		Schema::dropIfExists( 'team_question' );
	}
}

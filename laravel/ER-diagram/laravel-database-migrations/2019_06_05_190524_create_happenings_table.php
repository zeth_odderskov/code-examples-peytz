<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHappeningsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up(){
		Schema::create( 'happenings', function( Blueprint $table ){
			$table->bigIncrements( 'id' );
			$table->timestamps();
			$table->string( 'title' );
			$table->dateTime('start' );
			$table->dateTime('end' );
			$table->unsignedBigInteger('team_id' )->nullable();
			$table->unsignedBigInteger('user_id' );
			$table->boolean('is_team_happening' );
			$table->string('type' );
			$table->softDeletes();
		} );
		Schema::table( 'happenings', function( $table ){
			$table->foreign( 'team_id' )->references( 'id' )->on( 'teams' );
			$table->foreign( 'user_id' )->references( 'id' )->on( 'users' );
		});
	}





	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down(){
		Schema::dropIfExists( 'happenings' );
	}
}

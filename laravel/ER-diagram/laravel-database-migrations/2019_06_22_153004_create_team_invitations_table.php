<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamInvitationsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up(){
		Schema::create( 'team_invitations', function( Blueprint $table ){
			$table->bigIncrements( 'id' );
			$table->timestamps();
			$table->string( 'invited_email' );
			$table->unsignedBigInteger( 'team_id' );
			$table->unsignedBigInteger('user_id')->nullable();
			$table->unsignedBigInteger( 'invited_by' );
			$table->dateTime( 'approved_at' )->nullable();
			$table->json( 'role_ids' );
			$table->text( 'invitation_message' )->nullable();
			$table->text( 'response_message' )->nullable();
			$table->dateTime( 'expires_at' )->nullable();
			$table->dateTime( 'settled' )->nullable();
			$table->json('invitation_edits')->nullable();
			$table->softDeletes();
		} );
		Schema::table( 'team_invitations', function( Blueprint $table){
			$table->foreign( 'team_id' )->references( 'id' )->on( 'teams' );
			$table->foreign( 'invited_by' )->references( 'id' )->on( 'users' );
			$table->foreign('user_id')->references('id')->on('users');
		});
	}





	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down(){
		Schema::dropIfExists( 'team_invitations' );
	}
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHappeningInjuryUserTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up(){
		Schema::create( 'happening_injury_user', function( Blueprint $table ){
			$table->bigIncrements( 'id' );
			$table->timestamps();
			$table->unsignedBigInteger('user_id' );
			$table->unsignedBigInteger('happening_id' );
			$table->unsignedBigInteger('injury_id' );
			$table->bigInteger('pain_value' )->nullable();
			$table->text('user_comment' )->nullable();
			$table->text('coach_comment' )->nullable();
			$table->softDeletes();
		} );

		Schema::table( 'happening_injury_user', function( Blueprint $table ){
			$table->foreign( 'user_id' )->references('id')->on('users');
			$table->foreign( 'happening_id' )->references('id')->on('happenings');
			$table->foreign( 'injury_id' )->references('id')->on('injuries');
		});
	}





	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down(){
		Schema::dropIfExists( 'happening_injury_user' );
	}
}

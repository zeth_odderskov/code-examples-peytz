<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up(){
		Schema::create( 'roles', function( Blueprint $table ){
			$table->bigIncrements( 'id' );
			$table->timestamps();
			$table->string( 'title' );
			$table->bigInteger('permission_level')->default( 0 );
			$table->unsignedBigInteger( 'team_id' )->nullable();
			$table->unsignedBigInteger('user_id')->nullable();
			$table->boolean( 'default' )->default( false );

			$table->boolean('do_everything')->default( false );
			$table->boolean('change_team_details')->default( false );
			$table->boolean('see_team_trends')->default( false );
			$table->boolean('see_missing_answers')->default( false );
			$table->boolean('create_team_event')->default( false );
			$table->boolean('see_team_injuries')->default( false );
			$table->boolean('invite_to_team')->default( false );
			$table->softDeletes();
		} );

		Schema::table( 'roles', function( Blueprint $table ) {
			$table->foreign( 'team_id' )->references( 'id' )->on( 'teams' );
			$table->foreign( 'user_id' )->references( 'id' )->on( 'users' );
		});
	}





	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down(){
		Schema::dropIfExists( 'roles' );
	}
}

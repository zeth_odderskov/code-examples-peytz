<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up(){
		Schema::create( 'questions', function( Blueprint $table ){
			$table->bigIncrements( 'id' );
			$table->timestamps();
			$table->string( 'title' );
			$table->text( 'description' );
			$table->string( 'answer_type' )->default( 'range' );
			$table->unsignedBigInteger( 'lower_boundary' )->nullable();
			$table->unsignedBigInteger( 'upper_boundary' )->nullable();
			$table->unsignedBigInteger( 'user_id' )->nullable();
			$table->unsignedBigInteger( 'team_id' )->nullable();
			$table->boolean( 'duration_required' );
			$table->json( 'default_asked_upon' )->nullable();
			$table->softDeletes();
			$table->boolean( 'default' )->default( false );
		} );
		Schema::table( 'questions', function( Blueprint $table ){
			$table->foreign( 'user_id' )->references( 'id' )->on( 'users' );
			$table->foreign( 'team_id' )->references( 'id' )->on( 'teams' );
		});
	}





	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down(){
		Schema::dropIfExists( 'questions' );
	}
}

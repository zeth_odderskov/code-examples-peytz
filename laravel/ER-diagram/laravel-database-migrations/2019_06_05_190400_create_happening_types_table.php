<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHappeningTypesTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up(){
		Schema::create( 'happening_types', function( Blueprint $table ){
			$table->id();
			$table->timestamps();
			$table->unsignedBigInteger( 'user_id' )->nullable();
			$table->unsignedBigInteger( 'team_id' )->nullable();
			$table->string( 'title' );
			$table->boolean( 'default' )->default( false );
			$table->softDeletes();
		} );

		Schema::table( 'happening_types', function( Blueprint $table ){
			$table->foreign( 'user_id' )->references( 'id' )->on( 'users');
			$table->foreign( 'team_id' )->references( 'id' )->on( 'teams');
		});
	}





	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down(){
		Schema::dropIfExists( 'happening_types' );
	}
}

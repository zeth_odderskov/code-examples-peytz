<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\HappeningQuestionUser;
use App\Helpers\ApiHelpers;
use App\Happening;
use App\Team;
use App\User;
use Carbon\Carbon;

use Cron\AbstractField;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class HappeningController extends Controller {
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return array
	 * @throws \Exception
	 */
	public function create( Request $request ){

		$request->validate( [
			'title'             => 'required',
			'start'             => 'required|date_format:"Y-m-d H:i"',
			'end'               => 'required|date_format:"Y-m-d H:i"',
			'user_id'           => 'required|integer|exists:users,id',
			'team_id'           => 'required|integer|exists:teams,id',
			'is_team_happening' => 'required|in:true,false',
			'type'              => 'required|integer',
		] );

		$happening_start = ApiHelpers::get_carbon_obj_from_string( $request['start'], true );
		$happening_end   = ApiHelpers::get_carbon_obj_from_string( $request['end'], true );

		if( $happening_start->isAfter( $happening_end ) ){
			abort( 400, 'End-time has to be after start-time' );
		}

		$user              = User::find( $request['user_id'] );
		$team              = Team::find( $request['team_id'] );
		$is_team_happening = ( strtolower( $request['is_team_happening'] ) == 'true' ? true : false );
		$type              = $request['type'];

		$this->authorize( 'manage', $team );

		if( ! $user->isOnTeam( $team ) ){
			return [
				'status'         => 422,
				'status_message' => 'The user is not on the passed team_id',
			];
		}

		$happening                    = new Happening();
		$happening->title             = $request->title;
		$happening->start             = $happening_start;
		$happening->end               = $happening_end;
		$happening->user_id           = $user->id;
		$happening->team_id           = $team->id;
		$happening->is_team_happening = $is_team_happening;
		$happening->type              = $type;
		$happening->save();

		$happening->askQuestionsForUsersOnTeam();
		$users_questions = $happening->usersQuestions();

		return [
			'data'           => [
				'users_questions' => $users_questions,
				'happening'       => $happening,
			],
			'status'         => 200,
			'status_message' => '',
		];
	}





	/**
	 * Display the specified resource.
	 *
	 * @param int $id
	 *
	 * @return array
	 * @throws \Exception
	 */
	public function show( Request $request ){

		$request->validate( [
			'id' => 'required|integer|exists:happenings,id',
		] );

		$happening_id = (int) $request['id'];
		$happening    = Happening::find( $happening_id );

		// Check if it's answered
		$happening_question_user = HappeningQuestionUser::where( [
			[ 'happening_id', '=', $happening_id ],
		] )->get();

		$answered = true;
		foreach( $happening_question_user as $item ) {
			if( is_null( $item->answer_id ) ){
				$answered = false;
			}
		}

		if( is_null( $happening ) ){

			return [
				'data'           => [
					'happening' => null,
				],
				'status'         => 404,
				'status_message' => 'The happening could not be found.',
			];
		}

		$happening->is_team_happening = ( $happening->is_team_happening === 1 ) ? true : false;
		$happening->answered          = $answered;
		$team                         = Team::find( $happening->team_id );

		return [
			'data'           => [
				'happening' => $happening,
				'team'      => $team,
			],
			'status'         => 200,
			'status_message' => '',
		];
	}





	/**
	 * Remove the specified resource from storage.
	 *
	 * @param int $id
	 *
	 * @return array
	 * @throws \Exception
	 */
	public function destroy( Request $request ){

		$request->validate( [
			'id' => 'required|integer|exists:happenings,id',
		] );

		$happening = Happening::find( $request['id'] );

		$delete_response = $happening->delete();

		if( $delete_response ){
			return [
				'status'         => 200,
				'status_message' => 'The happening was deleted.',
			];
		}

		return [
			'status'         => 400,
			'status_message' => 'Something went wrong! The happening was not deleted.',
		];
	}





	/**
	 * @param Request $request
	 *
	 * @return array
	 * @throws \Exception
	 */
	public function getHappeningsForUser( Request $request ){

		$request->validate( [
			'user_id'      => 'required|integer|exists:users,id',
			'starts_after' => 'required|date_format:"Y-m-d"',
			'ends_before'  => 'date_format:"Y-m-d"',
		] );

		$user_id      = (int) $request['user_id'];
		$starts_after = ApiHelpers::get_carbon_obj_from_string( $request['starts_after'] );

		// Ends before
		$ends_before = null;
		if( isset( $request['ends_before'] ) ){
			$ends_before = ApiHelpers::get_carbon_obj_from_string( $request['ends_before'] );
		}

		$team_user_relational_lines = DB::table( 'team_user' )->where( [
			[ 'user_id', $user_id ],
		] )->get();

		$team_ids = [];
		if( ! empty( $team_user_relational_lines ) ){
			foreach( $team_user_relational_lines as $relational_line ) {
				$team_ids[] = $relational_line->team_id;
			}
		}

		if( ! is_null( $ends_before ) ){
			$happenings = Happening::whereIn( 'team_id', $team_ids )
			                       ->where( [
				                       [ 'happening_start', '>', $starts_after ],
				                       [ 'happening_end', '<', $ends_before ],
			                       ] )->orderBy( 'happening_start', 'DESC' )->get();
		} else {
			$happenings = Happening::whereIn( 'team_id', $team_ids )
			                       ->where( [
				                       [ 'happening_start', '>', $starts_after ],
			                       ] )->orderBy( 'happening_start', 'DESC' )->get();
		}

		if( ! empty( $happenings ) ){
			foreach( $happenings as $happening ) {

				// Check if it's answered
				$happening_question_user = HappeningQuestionUser::where( [
					[ 'happening_id', '=', $happening->id ],
					[ 'asked_to_user_id', '=', $user_id ],
				] )->get();

				$answered = true;
				foreach( $happening_question_user as $item ) {
					if( is_null( $item->answer_id ) ){
						$answered = false;
					}
				}

				$happening->answered = $answered;
			}
		}

		return [
			'data'           => [
				'happenings' => $happenings,
			],
			'status'         => 200,
			'status_message' => '',
		];
	}





	/**
	 * Get happening for entire team
	 * @throws \Exception
	 */
	public function getHappeningsForTeam( Request $request ){

		$request->validate( [
			'team_id'      => 'required|integer|exists:teams,id',
			'starts_after' => 'required|date_format:"Y-m-d"',
			'ends_before'  => 'date_format:"Y-m-d"',
		] );

		$team_id      = (int) $request['team_id'];
		$starts_after = ApiHelpers::get_carbon_obj_from_string( $request['starts_after'] );

		// Ends before
		$ends_before = null;
		if( isset( $request['ends_before'] ) ){
			$ends_before = ApiHelpers::get_carbon_obj_from_string( $request['ends_before'] );
		}

		if( is_null( $ends_before ) ){
			$happenings = Happening::where( [
				[ 'team_id', $team_id ],
				[ 'happening_start', '>', $starts_after ],

			] )->orderBy( 'happening_start', 'DESC' )->get();
		} else {
			$happenings = Happening::where( [
				[ 'team_id', $team_id ],
				[ 'happening_start', '>', $starts_after ],
				[ 'happening_start', '<', $ends_before ],
			] )->orderBy( 'happening_start', 'DESC' )->get();
		}

		// Add the answered bool
		if( ! empty( $happenings ) ):
			foreach( $happenings as $happening ):

				// Check if it's answered
				$happening_question_user = HappeningQuestionUser::where( [
					[ 'happening_id', '=', $happening->id ],
				] )->get();

				$answered = true;
				foreach( $happening_question_user as $item ) {
					if( is_null( $item->answer_id ) ){
						$answered = false;
					}
				}

				$happening->answered = $answered;

			endforeach; // foreach( $happenings as $item ):
		endif; // if ( !empty( $happenings ) )

		return [
			'data'           => [
				'happenings' => $happenings,
			],
			'status'         => 200,
			'status_message' => '',
		];
	}





	/**
	 * @param Request $request
	 *
	 * @return array|null
	 * @throws \Exception
	 */
	public function getHappeningsWithUnansweredQuestions( Request $request ){

		$request->validate( [
			'user_id'      => 'required|integer|exists:users,id',
			'starts_after' => 'date_format:"Y-m-d"',
			'ends_before'  => 'date_format:"Y-m-d"',
		] );

		$user_id = (int) $request['user_id'];

		$starts_after = Carbon::parse( '1972-01-01' ); // Fallback
		if( ! empty( $request['starts_after'] ) ){
			$starts_after = ApiHelpers::get_carbon_obj_from_string( $request['starts_after'] );
		}

		$ends_before = Carbon::today(); // Fallback
		if( ! empty( $request['ends_before'] ) ){
			$ends_before = ApiHelpers::get_carbon_obj_from_string( $request['ends_before'] );
		}

		$happenings_with_unanswered_questions = new Collection();
		$happening_question_user = HappeningQuestionUser::where( [
			[ 'asked_to_user_id', '=', $user_id ],
			[ 'answer_id', '=', null ],
		] )->get();

		foreach( $happening_question_user as $item ) {
			$happening = Happening::find( $item->happening_id );
			if( ! is_null( $happening ) ){
				if( $happening->happening_start > $starts_after && $happening->happening_end < $ends_before ){
					$happening->answered = false; // Hardcoded. Joakim wanted it.
					if( ! $happenings_with_unanswered_questions->has( $happening->id ) ){
						$happenings_with_unanswered_questions->put( $happening->id, $happening );
					}
				}
			}
		}

		return [
			'data'           => [
				'happenings' => $happenings_with_unanswered_questions->values(),
			],
			'status'         => 200,
			'status_message' => '',
		];
	}
}

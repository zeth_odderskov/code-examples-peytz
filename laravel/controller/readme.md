# Laravel Controller

**Kunde:** Athleeto  
**Live-eksempel:** Dette projekt er fortsat under udarbejdelse

## Kort om opgaven

Dette er et eksempel på en controller, der gør et par forskellige ting:

 - **create-function:** Opretter en Happening og stiller alle deltagere en række spørgsmål.
 - **show():** Viser en Happening
 - **destroy():** Sletter en Happening
 - **getHappeningsForUser():** Henter alle Happenings for en bruger.
 - **getHappeningsForTeam():** Henter alle Happenings for et hold.
 - **getHappeningsWithUnansweredQuestions():** Henter alle Happeningsfor en bruger, hvor der er nogle ubesvarede spørgsmål.

## Hvad demonstrerer denne kode

Denne kode demonstrerer lidt standard Laravel-skills:

 - request-validation
 - policies og authorization (hvilket jeg har forsøgt at løse på lidt forskellige måder)
 - code style

Jeg har med vilje ikke poleret koden her, så I får et mere realistisk billede af mit nuværende Laravel-niveau.  
Der er flere steder i denne kode, hvor jeg gerne ville have brugt tid på at refaktorere.  
Og på sigt når jeg jo et niveau, hvor koden bliver pænere i første omgang. :-)



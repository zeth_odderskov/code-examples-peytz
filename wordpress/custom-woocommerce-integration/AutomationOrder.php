<?php




class AutomationOrder {
	private $order_id;
	private $order;
	private $order_acf;
	private array $order_lines = [];
	private int $pick_up_store_id = 0;
	private int $pick_up_department_id = 0;
	private $shipping_method;
	private $shipping_method_id;
	private int $last_performed_step = 0;
	private int $step_in_progress = 0;
	private int $final_internal_destination = 0;
	private string $pur_and_part_picking_algorithm = '';
	private array $automation_flow = [];
	private array $initial_automation_products = [];
	private array $addon_parts_gross_list = [];
	private bool $manual_handling_enabled = false;
	private array $reasons_for_manual_handling = [];
	private bool $addons_handled = false;
	private string $addon_algorithm = '';





	public function __construct( $order ){
		$order_acf = get_fields( $order->get_id() );
		$this->setOrderAcf( $order_acf );
		$this->setOrderId( $order->get_id() );
		$this->setOrder( $order );
		$this->setShippingInfo();
	}




	public function loadInitialInfo(){
		$order = $this->getOrder();
		$gm_step = @( get_post_meta( $order->get_id(), 'gm_step', true ) ) ?: 0;
		$this->setLastPerformedStep( intval( $gm_step ) );

		$this->loadOrderLines();

		$this->fetchInitialAutomationProductsFromDb();
		$this->fetchFinalInternalDestinationFromDb();
		$this->fetchCalcAndSetupAutomationFlow();

		$automation_algorithm = get_post_meta( $order->get_id(), 'gm_automation_product_selection_algorithm', true );
		$this->setPurAndPartPickingAlgorithm( $automation_algorithm );

		$addon_algorithm = get_post_meta( $order->get_id(), 'gm_addon_selection_algorithm', true );
		$this->setAddonAlgorithm( $addon_algorithm );
	}





	public function isPickup(): bool {
		$shipping_method_id = $this->getShippingMethodId();

		if( $shipping_method_id === 'local_pickup_plus' ){
			return true;
		}

		return false;
	}





	public function isShipping(): bool {
		$shipping_method_id = $this->getShippingMethodId();

		if( $shipping_method_id === 'free_shipping' ){
			return true;
		}

		if( $shipping_method_id === 'flat_rate' ){
			return true;
		}

		return false;
	}





	private function loadOrderLines(){

		// Reload order
		$order_id = $this->getOrderId();
		$wc_order = wc_get_order( $order_id );
		$this->setOrder( $wc_order );

		// Reload order_lines
		$order_lines = $this->order->get_items();
		if( !empty( $order_lines ) ){
			foreach( $order_lines as $order_line_id => $order_line_content ){
				$order_line = new AutomationOrderLine( $this, $order_line_content, $order_line_id );
				$this->addOrderLine( $order_line, $order_line_id );
			}
		}
	}






	private function fetchFinalInternalDestinationFromDb(){
		$final_internal_destination = get_post_meta( $this->getOrderId(), 'gm_final_internal_destination', true );
		if( !empty( $final_internal_destination ) ){
			$this->setFinalInternalDestination( $final_internal_destination );
		}
	}





	public function addWebshopOrderInGms(){
		$api_request = new GmsApiRequest();
		//$data = $this->buildFullJson();
		$json_builder = new OrderJsonBuilder( $this );
		$data = $json_builder->buildFullJson();
		$extra_wrap = [
			'value' => base64_encode( json_encode( $data ) )
		];
		$json_encoded_extra_wrap = json_encode( $extra_wrap );
		$endpoint = 'http://irepsystem.dk/?id=api?key=32477262?command=addWebshopOrder';
		$protocol = 'POST';
		$api_request->setupCurl( $endpoint, $protocol, $json_encoded_extra_wrap );
		$api_request->executeCurl();
	}





	public function updateCustomerInfoInGms(){
		$api_request = new GmsApiRequest();
		$json_builder = new OrderJsonBuilder( $this );
		$data = $json_builder->buildFullJson();
		//$data = $this->buildFullJson();
		$extra_wrap = [
			'value' => base64_encode( json_encode( $data ) )
		];
		$json_encoded_extra_wrap = json_encode( $extra_wrap );
		$endpoint = 'http://irepsystem.dk/?id=api?key=32477262?command=updateCustomerInfoOnWebshopOrder';
		$protocol = 'POST';
		$api_request->setupCurl( $endpoint, $protocol, $json_encoded_extra_wrap );
		$api_request->executeCurl();
	}





	public function getAutomationProduct( $full_pur_part_id ){
		$automation_products = $this->getAutomationProducts();
		if( array_key_exists( $full_pur_part_id, $automation_products ) ){
			return $automation_products[ $full_pur_part_id ];
		}
		return false;
	}





	private function setInfoOnAutomationProduct( $full_pur_part_id, $key_to_update, $value ){
		$automation_product = $this->getAutomationProduct( $full_pur_part_id );
		if( $automation_product ){
			$automation_product->updateInfo( $key_to_update, $value );
		}
	}






	private function canStepRun( $step_to_check ): bool{
		$automation_flow = $this->getAutomationFlow();
		$has_errors = false;
		$has_all_previous_steps_been_completed = true;
		foreach( $automation_flow as $step_number => $step ){

			if( !empty( $step['errors'] ) ){
				$has_errors = true;
			}

			if( $step_to_check > $step_number ){
				if( ! $step['completed'] ){
					$has_all_previous_steps_been_completed = false;
				}
			}
		}

		if( ! $has_all_previous_steps_been_completed ){
			$this->addStepValidationError(  'Forrige skridt var ikke gennemført. ', $step_to_check );
			return false;
		}

		if( $has_errors ){
			$this->addStepValidationError( 'Der var nogle fejl, i nogle af de tidligere skridt. ', $step_to_check );
			return false;
		}

		return true;
	}





	public function runStep( int $step_number ){


		// 'PUR/Tilbehør valgt'
		if( $step_number == 20 && $this->canStepRun( $step_number ) ){
			$this->setStepInProgress( 20 );

			// Forced chosen products
			if( $this->isMissingAutomationProducts() ){

				if( empty( $this->getStepValidationErrors() ) ){
					$this->checkForManuallySetParts();
				}

				if( empty( $this->getStepValidationErrors() ) ){
					$this->checkForManuallySetPurs();
				}

				if( empty( $this->getStepValidationErrors() ) ){
					if( ! $this->isMissingAutomationProducts() ){
						$this->checkAndSetFinalInternalDestination();
					}
				}
			}

			// Automation Selection
			if( empty( $this->getStepValidationErrors() ) ){
				if( $this->isMissingAutomationProducts() ){
					$this->calcAndSetMissingAutomationProducts();
				}
			}

			// Validate chosen products
			$chosen_products_are_valid = $this->validateChosenPurAndParts();
			if( $chosen_products_are_valid ){
				$this->verifyAndSetMetaLinesOnOrderLines();
				$this->verifyAndSetMetaLinesOnShippingLines();
			}
			$this->markStepAsCompleted( 20 );
		}


		// Sampakningsprodukter
		if( $step_number == 30 && $this->canStepRun( $step_number ) ){
			$this->setStepInProgress( 30 );

			$is_addons_handled = $this->isAddonsHandled();
			if( ! $this->isAddonsHandled() ){
				$this->calcAndSetMissingAddonProduct();
			}

			$this->markStepAsCompleted( 30 );
		}



		// 'Indsæt køberinfo i GMS'
		if( $step_number == 60 && $this->canStepRun( $step_number ) ){
			$this->setStepInProgress( 60 );

			$errors = [];
			if( $this->isMissingAutomationProducts() ){
				$error = 'Der mangler at bliver valgt PUR/Tilbehør.';
				$this->addStepValidationError( $error );
				$errors[] = $error;
			}

			if( ! $this->isAddonsHandled() ){
				$error = 'Kørslen der vælger sampakningsprodukter var ikke kørt (eller også indeholder den fejl).';
				$this->addStepValidationError( $error );
				$errors[] = $error;
			}

			if( empty( $errors ) ){
				$this->addWebshopOrderInGms();
				update_post_meta( $this->getOrderId(), 'gm_automation_status', 'sent' );
			} else {
				$subject = 'Ordre ' . $this->getOrderId() . ' fejlet i forsøget på at blive sendt til GMS';
				$message = '';
				foreach( $errors as $key => $error ){
					$message .= $error;
					if( array_key_last( $errors ) !== $key ){
						$message .= " \n ";
					}
				}
				DevHelpers::sendMail( $subject, $message, 'wc-error' );
			}

			$this->markStepAsCompleted( 60 );
		}
	}





	private function checkAndSetFinalInternalDestination(){

		if( $this->isPickup() ){
			$pickup_department_id = $this->getPickupDepartmentId();
			$this->setFinalInternalDestination( $pickup_department_id );
		} else {
			$automation_products = $this->getAutomationProducts();
			$sold_from_department_ids = [];
			foreach( $automation_products as $automation_product ){
				$sold_from_department_id = $automation_product->getSoldFromDepartmentId();
				if( ! in_array( $sold_from_department_id, $sold_from_department_ids ) ){
					$sold_from_department_ids[] = $sold_from_department_id;
				}
			}

			if( count( $sold_from_department_ids ) === 1 ){
				$this->setFinalInternalDestination( $sold_from_department_ids[0] );
			}
		}
	}





	public function getOrderLineFromPartId( $part_id_to_find ){
		$order_lines = $this->getOrderLines();
		foreach( $order_lines as $order_line_id => $order_line ){
			$parsed_meta_data = $order_line->getParsedMetaData();
			if( array_key_exists( 'PART', $parsed_meta_data ) ){
				$part_values = $parsed_meta_data['PART'];
				foreach( $part_values as $full_part_id ){
					$parsed_part_id = GmsAccessory::parseFullPartId( $full_part_id );
					$temp_part_id = 'PART' . $parsed_part_id[ 'part_id' ];
					if( $temp_part_id == $part_id_to_find ){
						return $order_line;
					}
				}
			}
		}
		return null;
	}





	public function getOrderLineFromOrderLineId( $order_line_id_to_find ){
		$order_lines = $this->getOrderLines();
		foreach( $order_lines as $order_line_id => $order_line ){
			if( $order_line_id === $order_line_id_to_find ){
				return $order_line;
			}
		}
		return null;
	}





	private function parseAutomationProducts( $serialized_string ): array {
		$returned_arr = [];
		$unserialized_arr = unserialize( $serialized_string );
		if( !empty( $unserialized_arr ) ){
			foreach( $unserialized_arr as $semi_full_pur_part_id => $item_arr ){
				$full_pur_part_id = $item_arr[ 'full_pur_part_id' ];
				$order_line_id = $item_arr['order_line_id'];
				$order_line = $this->getOrderLineFromOrderLineId( $order_line_id );

				if( $order_line ){

					// PART
					if( str_contains( $full_pur_part_id, 'PART' ) ){
						$parsed_full_pur_part_id = GmsAccessory::parseFullPartId( $full_pur_part_id );
						$part_id = $parsed_full_pur_part_id[ 'part_id' ];
						$quantity = $parsed_full_pur_part_id[ 'quantity' ];
						$part = GmsAccessory::createFromPartId( $part_id );
						$order_line->addAutomationPart( $full_pur_part_id, $part, false, $quantity );
					}

					// PUR
					if( str_contains( $full_pur_part_id, 'PUR' ) ){
						$pur_id  = str_replace( 'PUR', '', $full_pur_part_id );
						$gms_pur = GmsPur::createFromPurId( $pur_id );
						$order_line->addAutomationPur( $full_pur_part_id, $gms_pur );

						// Set order_line_ids_for_addons
						$automation_products = $order_line->getAutomationProducts();
						$order_line_ids_for_addons = $item_arr[ 'order_line_ids_for_addons' ];
						$automation_products[ $full_pur_part_id ]->setOrderLineIdsForAddons( $order_line_ids_for_addons );
					}
				}

			}
		}
		return $returned_arr;
	}





	private function fetchInitialAutomationProductsFromDb(){
		$initial_automation_products = get_post_meta( $this->getOrderId(), 'gm_automation_products', true );

		if( !empty( $initial_automation_products ) ){
			$parsed_automation_products = $this->parseAutomationProducts( $initial_automation_products );
			if( !empty( $parsed_automation_products ) ){
				$this->setInitialAutomationProducts( $parsed_automation_products );
			}
		}
	}





	public function addOrderNote( $note ){
		$order = $this->getOrder();
		$order->add_order_note( $note );
	}




	private function markStepAsCompleted( $step_number ){
		$this->setStepInProgress( 0 ); // 0 is that no step is in progress
		$automation_flow = $this->getAutomationFlow();
		if( empty( $automation_flow[ $step_number ][ 'errors' ] ) ){
			$automation_flow[ $step_number ][ 'completed' ] = true;
		} else {
			$error_str = '';
			$errors = $automation_flow[ $step_number ][ 'errors' ];
			foreach( $errors as $key => $error ){
				$error_str .= "\n ### " . $error;
			}
			$this->addOrderNote( 'Forsøgte at markere AutomationStep-'.$step_number.' som færdig, men der var følgende fejl: ' . $error_str );
		}
		$this->setAutomationFlow( $automation_flow );
	}





	private function clearAutomationFlowErrors( $automation_flow ){
		foreach( $automation_flow as $step_number => &$step ){
			$step['errors'] = [];
		}
		return $automation_flow;
	}





	private function fetchCalcAndSetupAutomationFlow(){
		$automation_flow = get_post_meta( $this->getOrderId(), 'gm_automation_flow', true );
		if( ! empty( $automation_flow ) ){
			$automation_flow = unserialize( $automation_flow );
		} else {
			$automation_flow = [
				10 => [
					'name' => 'Automation opsætning færdig',
					'completed' => true,
					'errors' => []
				],
				20 => [
					'name' => 'PUR/Tilbehør valgt',
					'completed' => false,
					'errors' => []
				],
				30 => [
					'name' => 'Vælg sampakningsprodukter',
					'completed' => false,
					'errors' => []
				],
				60 => [
					'name' => 'Send ordre til GMS',
					'completed' => false,
					'errors' => []
				],
			];
		}

		$automation_flow = $this->clearAutomationFlowErrors( $automation_flow );
		$this->setAutomationFlow( $automation_flow );
	}





	public function addChosenPurInfo( $full_pur_id, $key, $value ){
		$automation_products = $this->getAutomationProducts();
		if( !array_key_exists( $full_pur_id, $automation_products ) ){
			$automation_products[ $full_pur_id ] = [];
		}

		$automation_products[ $full_pur_id ][ $key ] = $value;
		$this->setAutomationProducts( $automation_products );
	}





	public function addChosenAccessoryInfo( $full_part_id, $key, $value ){
		$automation_products = $this->getAutomationProducts();
		if( !array_key_exists( $full_part_id, $automation_products ) ){
			$automation_products[ $full_part_id ] = [];
		}

		$automation_products[ $full_part_id ][ $key ] = $value;
		$this->setAutomationProducts( $automation_products );
	}






	public function isMissingAutomationProducts(): bool {
		$is_missing_chosen_purs_or_accessories = false;

		foreach( $this->getOrderLines() as $order_line ){
			if( ! $order_line->chosenAutomationProductsAreSaturated() ){
				$is_missing_chosen_purs_or_accessories = true;
			}
		}

		return $is_missing_chosen_purs_or_accessories;
	}






	public function validateChosenPurAndParts(): bool {
		$is_valid = true;

		foreach( $this->getOrderLines() as $order_line_id => $order_line ){

			// created_from_gms_object
			$created_from_gms_object = $order_line->getCreatedFromGmsObject();

			// Validate PUR
			if( $created_from_gms_object == 'PUR' ){
				if( ! $order_line->chosenAutomationPursAreSaturated() ){
					$this->addStepValidationError( 'Antallet af valgte PUR passede ikke med antallet der er solgt, for ordrelinje: ' . $order_line_id );
					$is_valid = false;
				}
			}

			// Validate PARTS
			if( $created_from_gms_object == 'PART' ){
				if( ! $order_line->chosenAutomationPartsAreSaturated() ){
					$this->addStepValidationError( 'Antallet af det valgte tilbehør passede ikke med antallet der er solgt, for ordrelinje: ' . $order_line_id );
					$is_valid = false;
				}
			}

			if( empty( $created_from_gms_object ) ){
				$this->addStepValidationError( 'Created_from_gms_object var blank' );
				$is_valid = false;
			}
		}

		return $is_valid;
	}






	public function deleteAllGreenMindMetaLines(){
		delete_post_meta( $this->getOrderId(), 'gm_automation_product_selection_algorithm' );
		delete_post_meta( $this->getOrderId(), 'gm_addon_selection_algorithm' );
		delete_post_meta( $this->getOrderId(), 'gm_final_internal_destination' );
		delete_post_meta( $this->getOrderId(), 'gm_automation_products' );
		delete_post_meta( $this->getOrderId(), 'gm_automation_flow' );
		update_post_meta( $this->getOrderId(), 'gm_automation_status', 'ignore' );
		foreach( $this->getOrderLines() as $order_line ){
			$order_line->deleteAllGreenMindMetaLines();
		}
		delete_post_meta( $this->getOrderId(), 'gm_manual_handling_enabled' );
		$this->getOrder()->apply_changes();

		// Delete meta from raw order lines
		$raw_wc_order_lines = $this->order->get_items();
		if( !empty( $raw_wc_order_lines ) ){
			$keys_to_delete = AutomationHelpers::getMetaKeysThatShouldBeDeletedOnReset();
			foreach( $raw_wc_order_lines as $order_line_id => $order_line_content ){
				$order_line_metas = $order_line_content->get_meta_data();
				foreach( $order_line_metas as $counter => $m ){
					if( in_array( $m->key, $keys_to_delete ) ){
						wc_delete_order_item_meta( $order_line_id, $m->key );
					}
				}

			}
		}

	}





	public function save(){
		update_post_meta( $this->getOrderId(), 'gm_automated_operations_has_finished', 1 );

		// Automation flow
		$automation_flow = $this->getAutomationFlow();
		update_post_meta( $this->getOrderId(), 'gm_automation_flow', serialize( $automation_flow ) );

		// PUR / Accessory array
		$gm_automation_products = $this->getAutomationProducts();
		foreach( $gm_automation_products as &$gm_automation_product ){
			$gm_automation_product = $gm_automation_product->toArray();
		}
		update_post_meta( $this->getOrderId(), 'gm_automation_products', serialize( $gm_automation_products ) );

		$algorithm = $this->getPurAndPartPickingAlgorithm();
		update_post_meta( $this->getOrderId(), 'gm_automation_product_selection_algorithm', $algorithm );

		$addon_algorithm = $this->getAddonAlgorithm();
		update_post_meta( $this->getOrderId(), 'gm_addon_selection_algorithm', $addon_algorithm );
		update_post_meta( $this->getOrderId(), 'gm_final_internal_destination', $this->getFinalInternalDestination() );
		update_post_meta( $this->getOrderId(), 'gm_manual_handling_enabled', $this->isManualHandlingEnabled() );

		if( $this->isManualHandlingEnabled() ){
			//
			$reasons_for_manual_handling = $this->getReasonsForManualHandling();
			if( !empty( $reasons_for_manual_handling ) ){
				$reason_str = '';
				foreach( $reasons_for_manual_handling as $key => $reason ){
					$reason_str .= $reason;
					if( array_key_last( $reasons_for_manual_handling ) !== $key ){
						$reason_str .= ' || ';
					}
				}
				$this->addOrderNote( 'MANUEL HÅNDTERINGSÅRSAG: ' . $reason_str );

				// Mail
				$subject = "Manuel håndtering slået til på ordre: " . $this->getOrderId();
				$message = "Link til ordre: " . get_site_url() . "/wp-admin/post.php?post=". $this->getOrderId() ."&action=edit \n";
				$message .= "Årsag(er): " . $reason_str;
				DevHelpers::sendMail( $subject, $message, 'wc-help' );
			}
		}

		$this->getOrder()->save();
	}






	public function saveAndDie(){
		$this->save();
		wp_safe_redirect( admin_url('/post.php?post='. $this->getOrderId() .'&action=edit') );
		exit();
	}






	private function getSingleChosenGmsAccessoryFromChosenAccessoryContent( $chosen_accessory_content ){
		throw new Exception( 'getSingleChosenGmsAccessoryFromChosenAccessoryContent shouldnt be used' );
		$quantity = @( $chosen_accessory_content['to_be_picked'] ) ?: 0;
		$order_line_id = @( $chosen_accessory_content['order_line_id'] ) ?: '';
		return [
			'quantity' => $quantity,
			'shipping_status' => '',
			'track_n_trace_internal' => '',
			'track_n_trace_to_customer' => '',
			'order_line_id' => $order_line_id
		];
	}





	private function setChosenGmsPur( $order_line_id, $chosen_gms_purs_for_order_line ){
		$order_lines = $this->getOrderLines();
		$chosen_gms_purs_quantity_for_order_line = count( $chosen_gms_purs_for_order_line );
		$quantity_sold = $order_lines[ $order_line_id ]->getQuantity();
		if( $chosen_gms_purs_quantity_for_order_line == $quantity_sold ){
			$order_lines[ $order_line_id ]->addSeveralAutomationPursFromRawGmsPurArray( $chosen_gms_purs_for_order_line );
		} else {
			$error = 'Antallet af valgte PUR ('. $chosen_gms_purs_quantity_for_order_line .') og antallet solgt ('. $quantity_sold .') stemte ikke på ordrelinjen med id: ' . $order_line_id . '.  //  ' ;
			// $this->addOrderNote( $comment );
			$this->addStepValidationError( $error );
		}
	}






	private function verifyAndSetMetaLinesOnOrderLines(){
		$order_lines = $this->getOrderLines();
		foreach( $order_lines as $order_line_id => $order_line ){
			$order_line->verifyAndSetMetalinesForAutomationProducts();
		}
	}





	private function verifyAndSetMetaLinesOnShippingLines(){
		$wc_order = $this->getOrder();
		$shipping_items = $wc_order->get_items( 'shipping' );
		$shipping_id = $this->getShippingMethodId();

		if( in_array( $shipping_id, [ 'flat_rate', 'free_shipping' ] ) ){
			// Check if it's already set
			$cost = false;
			foreach( $shipping_items as $shipping_item_id => $shipping_item ){
				$shipping_metas = $shipping_item->get_meta_data();
				foreach( $shipping_metas as $shipping_meta ){
					$key = $shipping_meta->key;
					if( $key == '_cost' ){
						$cost = $shipping_meta->value;
					}
				}
			}

			// Set it, if it wasnt
			$shipping_cost = 44;
			if( $cost === false ){
				foreach( $shipping_items as $shipping_item_id => $shipping_item ){
					$shipping_item->add_meta_data( '_cost', $shipping_cost );
				}
			}
		}
	}





	private function getIdealAddonArray(): array{
		$returned_array = [];
		$automation_purs = $this->getAutomationPurs();
		foreach( $automation_purs as $automation_pur ){
			$addon_part_ids = $automation_pur->getAddonProductIds();

			// No addons to be selected
			if( count( $addon_part_ids ) === 1 && $addon_part_ids[0] === '-' ){
				continue;
			}

			$pref_dep_id = $automation_pur->getSoldFromDepartmentId();
			foreach( $addon_part_ids as $addon_part_id ){
				$addon_part_id = 'PART' . $addon_part_id;
				if( !array_key_exists( $pref_dep_id, $returned_array ) ){
					$returned_array[ $pref_dep_id ] = [];
				}

				if( ! array_key_exists( $addon_part_id, $returned_array[ $pref_dep_id ] ) ){
					$returned_array[ $pref_dep_id ][ $addon_part_id ] = [];
				}
				$returned_array[ $pref_dep_id ][ $addon_part_id ][] = $automation_pur;
			}
		}

		// Will have this format
		//		$ideal_dep_and_qty = [
		//			20 => [             // Dep
		//				PART11098 => [
		//          AutomationProduct,
		//          AutomationProduct,
		//        ]
		//			],
		//			16 => [             // Dep
		//				PART11098 => [
		//          AutomationProduct,
		//        ]
		//			],
		//		];

		return $returned_array;
	}





	private function incQtyOnCurrentyPicked( $currently_picked, $addon_gross_list, $addon_id, $dep_id ): array {

		// Decrease stock from addon_gross_list,
		// to avoid two products 'picking' the same addon
		if( array_key_exists( $addon_id, $addon_gross_list ) ){
			$stock = $addon_gross_list[ $addon_id ]->getStock();
			if(
				array_key_exists( $dep_id, $stock ) &&
				$stock[ $dep_id ] > 0
			){
				$stock[ $dep_id ]--;
				$addon_gross_list[ $addon_id ]->setStock( $stock );
			}
		}

		// Add the pick to currently_picked
		if( ! array_key_exists( $addon_id, $currently_picked ) ){
			$currently_picked[ $addon_id ] = 1;
		} else {
			$currently_picked[ $addon_id ]++;
		}

		return [
			'currently_picked' => $currently_picked,
			'addon_gross_list' => $addon_gross_list
		];
	}





	private function addToReturnedArray( $dep_id, $addon_id, $automation_product, $returned_array ): array{
		if( ! array_key_exists( $dep_id, $returned_array ) ){
			$returned_array[ $dep_id ] = [];
		}

		if( ! array_key_exists( $addon_id, $returned_array[ $dep_id ] ) ){
			$returned_array[ $dep_id ][ $addon_id ] = [];
		}

		$full_pur_part_id = $automation_product->getFullPurPartId();
		if( ! array_key_exists( $full_pur_part_id, $returned_array[ $dep_id ][ $addon_id ] ) ){
			$returned_array[ $dep_id ][ $addon_id ][ $full_pur_part_id ] = $automation_product;
		}

		return $returned_array;
	}






	private function getFallbackAddonArray(): array {
		$returned_array = [];
		$currently_picked = []; // addon_ids as keys
		$automation_products = $this->getAutomationProducts();
		$addon_gross_list = $this->getAddonPartsGrossList();

		foreach( $automation_products as $full_pur_part_id => $automation_product ){
			if( $automation_product->isPur() ){
				$addons = $automation_product->getAddonProductIds();

				if( count( $addons ) === 1 && $addons[0] === '-' ){
					continue;
				}

				$pref_dep_id = $automation_product->getSoldFromDepartmentId();

				foreach( $addons as $addon_id ){

					// Check if part is sync'red
					if( $this->isPartSyncedFromGms( $addon_id, $automation_product->getFullPurPartId() ) ){

						$addon = $addon_gross_list[ $addon_id ];
						$addon_stock_for_pref_dep = $addon->getStockForDepartment( $pref_dep_id );
						$already_picked = @( $currently_picked[ $addon_id ] ) ?: 0;

						if( $addon_stock_for_pref_dep > $already_picked ){
							// It is in stock for department, where the PUR was sold,
							// and not all of them have been picked away for this order. ... So one should be taken.

							$tmp = $this->incQtyOnCurrentyPicked( $currently_picked, $addon_gross_list, $addon_id, $pref_dep_id );
							$currently_picked = $tmp['currently_picked'];
							$addon_gross_list = $tmp['addon_gross_list'];
							$returned_array = $this->addToReturnedArray( $pref_dep_id, $addon_id, $automation_product, $returned_array );

						} else {
							// It is NOT in stock for department, where the PUR was sold,
							// So it should just find one and take that.

							//$stock_on_all_dep_for_addon_id = $this->getStockOnAllAddonPartGrossList( $addon_id );
							$stock_in_all_dep = $addon->getStock();

							arsort( $stock_in_all_dep );
							$highest_stock_dep = array_key_first( $stock_in_all_dep );
							$highest_stock_qty = $stock_in_all_dep[ $highest_stock_dep ];

							if( $highest_stock_qty > 0 ){
								// There is something in stock, in another department
								$currently_picked = $this->incQtyOnCurrentyPicked( $currently_picked, $addon_gross_list, $addon_id, $highest_stock_dep );
								$returned_array = $this->addToReturnedArray( $highest_stock_dep, $addon_id, $automation_product, $returned_array );
							} else {
								// There is NOT something in stock, in another department
								// Manual handling
								$error = 'Der er ikke nok på lager af tilbehør: ' . $addon_id . ' - der skal følge med ' . $automation_product->getFullPurPartId();
								$this->addStepValidationError( $error );
								$this->setManualHandlingEnabled( true );
								$this->addReasonForManualHandling( $error );
							}
						}
					} else {
						$this->addStepValidationError( 'Produktet med tilbehør-ID: ' . $addon_id . ' er ikke oprettet i WooCommerce. Opret det, nulstil det hele og kør skridtene igen fra start. ' );
					}

				}
			}
		}

		// Will have this format
		//		$ideal_dep_and_qty = [
		//			20 => [             // Dep
		//				11098 => 1        // 1 is QTY
		//			],
		//			16 => [             // Dep
		//				11098 => 1        // 1 is QTY
		//			],
		//		];

		return $returned_array;
	}





	private function isPartSyncedFromGms( $part_id, $pur_id = '' ): bool{
		$parts = $this->getAddonPartsGrossList();
		if( ! array_key_exists( $part_id, $parts ) ){

			if( ! $this->isManualHandlingEnabled() ){

				$manual_handling_reason = 'Tilbehøret med ID: ' . $part_id . ' mangler at blive synkroniseret fra GMS til webshoppen. Automatisk håndtering standset. ';
				if( !empty( $pur_id ) ){
					$manual_handling_reason .= 'Det var PUR: ' . $pur_id . ' der var markeret til at have det tilbehør med som sampakningsprodukt';
				}

				$this->addStepValidationError( $manual_handling_reason );
				$this->setManualHandlingEnabled( true );
				$this->addReasonForManualHandling( $manual_handling_reason );
			}

			return false;
		}
		return true;
	}






	private function canIdealCaseBeUtilized( array $dep_part_id_and_qty_array, $addons_gross_list ){
		// $parts = $this->getAddonPartsGrossList();

		if( empty( $dep_part_id_and_qty_array ) ){
			return false;
		}

		foreach( $dep_part_id_and_qty_array as $dep_id => $part_id_and_qty ){
			foreach( $part_id_and_qty as $part_id => $automation_products ){
				$qty = count( $automation_products );

				// Is part sync'ed from GMS
				if( isset( $addons_gross_list[ $part_id ] ) ){
					$part_stock = $addons_gross_list[ $part_id ]->getStock();
					if( ! array_key_exists( $dep_id, $part_stock ) || $part_stock[ $dep_id ] < $qty ){
						return false;
					}
				} else {
					// Part isn't sync'ed from GMS
					$this->addStepValidationError( "It looks like that the part with part_id: " . $part_id . " isn't synced from GMS.");
					return false;
				}
			}
		}

		return true;
	}





	private function calcAndSetAddonPartGrossList(){
		$addon_parts_gross_list = [];
		$automation_purs = $this->getAutomationPurs();
		foreach( $automation_purs as $full_pur_part_id => $automation_pur ){
			$addon_part_ids = $automation_pur->getAddonProductIds();

			if( empty( $addon_part_ids ) ){
				$this->addStepValidationError( 'Sampakningsprodukter var ikke sat på model type id: ' . $automation_pur->getModelTypeId() . ' (' . $full_pur_part_id . ').');
				continue;
			}

			if( count( $addon_part_ids ) == 1 && $addon_part_ids[0] == '-' ) {
				continue;
			}

			foreach( $addon_part_ids as $short_part_id ){
				if( !array_key_exists( $short_part_id, $addon_parts_gross_list ) ){
					$part = GmsAccessory::createFromPartId( $short_part_id );
					$part_id = 'PART' . $short_part_id;
					if( $part ){
						$part->setAddonToFullPurId( $automation_pur->getFullPurPartId() );
						$addon_parts_gross_list[ $part_id ] = $part;
					} else {

						$error = 'Der var ikke overført tilbehøret med ID: ' . $part_id . ' fra GMS.';
						$this->addStepValidationError( $error );
						$this->setManualHandlingEnabled( true );
						$this->addReasonForManualHandling( $error );
					}
				}
			}

		}
		$this->setAddonPartsGrossList( $addon_parts_gross_list );
	}





	public function addAdditionalAddonToExistingOrderLine( $order_line, $full_part_id, $automation_pur ){
		$addon_to = $automation_pur->getFullPurPartId();
		$parsed_full_part_id = GmsAccessory::parseFullPartId( $full_part_id );
		$semi_full_part_id = $parsed_full_part_id['semi_full_part_id'];
		$part_id = $parsed_full_part_id[ 'part_id' ];
		$dep_id_to_find = $parsed_full_part_id[ 'department_id' ];
		$part = GmsAccessory::createFromPartId( $part_id );

		$order_line_id = $order_line->getOrderLineId();
		$raw_order_line = $order_line->getOrderLine();

		// Qty
		$current_qty = $raw_order_line->get_quantity();
		$new_quantity = intval( $current_qty ) + 1;
		$raw_order_line->set_quantity( $new_quantity );
		$order_line->setQuantity( $order_line->getQuantity() + 1 );
		$order_line->increaseQuantityForAutomationProduct( $semi_full_part_id );
		// wc_update_order_item_meta( $order_line_id, '_qty', $new_quantity );
		// wc_add_order_item_meta( $order_line_id, '_qty', $new_quantity );

		// Cost
		$cost = $raw_order_line->get_meta( '_cost' );
		$cost_for_single = floatval( $cost ) / $current_qty;
		$new_cost = $cost_for_single * $new_quantity;
		$order_line->updateOrderItemMeta( '_cost', $new_cost );

		// Add / Adjust semi_full_part_id
		$semi_full_part_id_exists = false;
		$parsed_meta_data = $order_line->getParsedMetaData();
		if( array_key_exists( 'PART', $parsed_meta_data ) ){
			foreach( $parsed_meta_data[ 'PART' ] as $temp_full_part_id ){
				$parsed_temp_full_part_id = GmsAccessory::parseFullPartId( $temp_full_part_id );
				$temp_semi_full_part_id = $parsed_temp_full_part_id[ 'semi_full_part_id' ];
				if( $temp_semi_full_part_id === $semi_full_part_id ){
					$semi_full_part_id_exists = true;
					$order_line->removeOrderItemMeta( 'PART', $temp_full_part_id );
					$order_line->addOrderItemMeta( 'PART', $semi_full_part_id . '#' . $new_quantity );
				}
			}
		}
		$new_parsed_meta_data = $order_line->getParsedMetaData();
		if( ! $semi_full_part_id_exists ){
			$order_line->addOrderItemMeta( 'PART', $full_part_id );
		}

		$order_line->addOrderItemMeta( '_addon-to', $addon_to );

		// Add orderLineId to AutomationPur (to be able to verify connection)
		$automation_pur->addOrderLineIdForAddons( $order_line_id );
		$order_line->addAutomationPart( $full_part_id, $part, true, 1 );

		// Save (to ensure that the line quantity is correct)
		$order_line->save();
	}






	public function addAddonOrderLineAndAutomationProduct( $part_id, $automation_pur, $dep_id ){
		$short_part_id = str_replace( 'PART', '', $part_id );
		$part_id = 'PART' . $short_part_id;

		$wc_product = AutomationHelpers::getWcProductFromPartId( $short_part_id);
		if( empty( $wc_product ) ){
			$this->addStepValidationError( 'Der blev ikke fundet noget produkt med part_id: ' . $part_id . '. Sampakningsprodukterne kan ikke blive samlet rigtigt.' );
		} elseif( is_array( $wc_product ) ){
			$product_ids = [];
			foreach( $wc_product as $single_product ){
				$product_ids[] = $single_product->get_id();
			}
			$this->addStepValidationError( 'More than one product was found with the part_id: ' . $part_id . '. Post ids: ' . implode( ', ', $product_ids ) . '. Sampakningsprodukterne kan ikke blive samlet rigtigt.' );
		} else {

			// One product found. Let's go baby!

			// Build variables and info
			$part = GmsAccessory::createFromPartId( $short_part_id );
			$cost_per_part = $part->getCostPrice();
			$addon_to = $automation_pur->getFullPurPartId();
			$full_part_id = $part_id . '_' . $dep_id . '#1';

			// Create order line
			$order_item_name = $wc_product->get_name();
			$args = [
				'order_item_name' => $order_item_name,
				'order_item_type' => 'line_item'
			];
			$order_line_id_for_addon = wc_add_order_item( $this->getOrderId(), $args );
			$automation_pur->addOrderLineIdForAddons( $order_line_id_for_addon );

			if( $order_line_id_for_addon ){
				wc_add_order_item_meta( $order_line_id_for_addon, '_qty', 1);
				wc_add_order_item_meta( $order_line_id_for_addon, '_product_id', $wc_product->get_id() );
				wc_add_order_item_meta( $order_line_id_for_addon, '_line_total', 0);
				wc_add_order_item_meta( $order_line_id_for_addon, '_line_tax', 0);
				wc_add_order_item_meta( $order_line_id_for_addon, '_line_subtotal', 0);
				wc_add_order_item_meta( $order_line_id_for_addon, '_line_subtotal_tax', 0);
				wc_add_order_item_meta( $order_line_id_for_addon, '_cost', $cost_per_part);
				wc_add_order_item_meta( $order_line_id_for_addon, '_addon-to', $addon_to );
//				wc_add_order_item_meta( $order_line_id_for_addon, '_addon-id', $part_id ); // Redundant
				wc_add_order_item_meta( $order_line_id_for_addon, 'PART', $full_part_id );
			}

			// Add the AutomationOrderLine
			$wc_order = $this->getOrder();
			$order_line_content = $wc_order->get_item( $order_line_id_for_addon );
			$order_line = new AutomationOrderLine( $this, $order_line_content, $order_line_id_for_addon );

			// Choose Automation Products
			$order_line->addAutomationPart( $full_part_id, $part, true );
			$this->addOrderLine( $order_line, $order_line_id_for_addon );

			// Save (to ensure that the line quantity is correct)
			$order_line->save();
		}
	}






	private function calcAndSetMissingAddonProduct(){

		// Calc addon gross list
		$this->calcAndSetAddonPartGrossList();

		// Ideally (from same department as PUR)
		if( ! $this->isAddonsHandled() ){
			$addon_gross_list = $this->getAddonPartsGrossList();
			$addon_gross_list = AutomationHelpers::adjustAddonGrossListForAlreadyChosenParts( $addon_gross_list, $this );
			$ideal_dep_part_and_qty = $this->getIdealAddonArray();
			if( $this->canIdealCaseBeUtilized( $ideal_dep_part_and_qty, $addon_gross_list ) ){
				$this->setAddonAlgorithm( 'best-case' );

				foreach( $ideal_dep_part_and_qty as $dep_id => $part_id_and_qty ){ // Department
					foreach( $part_id_and_qty as $part_id => $automation_products ){ // Part_id
						foreach( $automation_products as $automation_pur ){ // Automation_product
							$addon_gross_list = $automation_pur->pickSpecificAddon( $addon_gross_list, $dep_id );
							//$this->addAddonOrderLineAndAutomationProduct( $part_id, $automation_product, $dep_id );
						}
					}
				}

			}
		}

		// Free-for-all picking
		if( ! $this->isAddonsHandled() ){
			$automation_purs = $this->getAutomationPurs();
			$addon_gross_list = $this->getAddonPartsGrossList();
			$addon_gross_list = AutomationHelpers::adjustAddonGrossListForAlreadyChosenParts( $addon_gross_list, $this );
			if( !empty( $addon_gross_list ) ){
				$this->setAddonAlgorithm( 'free-for-all' );
				foreach( $automation_purs as $automation_pur ){
					$addon_gross_list = $automation_pur->pickRandomAddon( $addon_gross_list );
				}
			}
		}


		// Fallback - Addons should hopefully always be selected here.
		// ... but if they aren't, then something needs to happen.
		if( ! $this->isAddonsHandled() ){
			$addons_array = array_keys( $this->getAddonPartsGrossList() );
			if( ! is_array( $addons_array ) ){
				$addons_array = [];
			}
			$this->addStepValidationError( 'PUR-Tilbehør kunne ikke blive tilføjet. Det er muligvis noget med lager-beholdingen. Dette er de(t) valgte PUR-tilbehør: ' . implode( ', ', $addons_array ) );
		}
	}





	/**
	 * @throws Exception
	 */
	private function checkForManuallySetParts(){

		// Calc PARTS
		foreach( $this->getOrderLines() as $order_line ){
			if( ! $order_line->chosenAutomationProductsAreSaturated() ){
				if( $order_line->getType() == 'simple' ){

					$parsed_meta_data = $order_line->getParsedMetaData();
					if( array_key_exists( '_force-PART', $parsed_meta_data ) ){

						$is_valid = $order_line->validateForcedPartMetaData();

						if( $is_valid ){
							// Choose parts
							foreach( $parsed_meta_data['_force-PART'] as $forced_part_id ){
								$parsed_part_id = GmsAccessory::parseFullPartId( $forced_part_id );
								$quantity = $parsed_part_id['quantity'];
								$part = GmsAccessory::createFromPartId( $parsed_part_id['part_id'] );
								$full_part_id = $parsed_part_id['full_part_id'];
								$order_line->addAutomationPart( $full_part_id, $part, true, $quantity );
							}
						}

					}
				}
			}
		}
	}





	private function checkForManuallySetPurs(){

		// Calc PUR
		foreach( $this->getOrderLines() as $order_line ){
			if( ! $order_line->chosenAutomationProductsAreSaturated() ){
				if( $order_line->getType() == 'variation' ){

					$parsed_meta_data = $order_line->getParsedMetaData();
					if( array_key_exists( '_force-PUR', $parsed_meta_data ) ){

						$is_valid = $order_line->validateForcedPurMetaData();
						if( $is_valid ){
							foreach( $parsed_meta_data['_force-PUR'] as $full_pur_id ){
								$gms_pur = GmsPur::createFromPurId( $full_pur_id );
								$order_line->addAutomationPur( $full_pur_id, $gms_pur, true );
							}
						}
					}
				}
			}
		}
	}






	private function calcAndSetMissingAutomationProducts(){

		// Calc PUR
		$order_lines_for_pur = [];
		foreach( $this->getOrderLines() as $order_line ){
			if( ! $order_line->chosenAutomationProductsAreSaturated() ){
				if( $order_line->getType() == 'variation' ){
					$order_line->calcPossiblePurs();
					$order_lines_for_pur[ $order_line->getOrderLineId() ] = $order_line;
				}
			}
		}

		// Calc Accessories
		$order_lines_for_accessories = [];
		foreach( $this->getOrderLines() as $order_line ){
			if( ! $order_line->chosenAutomationProductsAreSaturated() ){
				if( $order_line->getType() == 'simple' ){
					$order_line->calcPossibleParts();
					$order_lines_for_accessories[ $order_line->getOrderLineId() ] = $order_line;
				}
			}
		}

		$product_picker = new AutomationProductPicker( $order_lines_for_pur, $order_lines_for_accessories, $this );
		$product_picker->findChosenPursAndParts();
	}






	public function displayOrderContent(){
		if( !empty( $this->getOrderLines() ) ){
			foreach( $this->getOrderLines() as $order_line ){
				$order_line->displayContent();
			}
		}
	}






	public static function sortGmsPursByDepartment( $department_and_gms_purs ){

		uasort( $department_and_gms_purs, function( $a, $b ){
			$a_count = count( $a );
			$b_count = count( $b );
			if( $a_count == $b_count ){

				if( $a_count > 0 ){
					$a_dep = @( $a[0]['location_department_id'] ) ?: 0;
					if( intval( $a_dep ) == 16 ){
						return -1;
					}

					$b_dep = @( $b[0]['location_department_id'] ) ?: 0;
					if( intval( $b_dep ) == 16 ){
						return 1;
					}
				}

				return 0;
			}

			return ( $a_count < $b_count ) ? 1 : -1;
		});
		return $department_and_gms_purs;
	}





	private function setShippingInfo(){
		// Shipping method
		$this->setShippingMethod( $this->order->get_shipping_method() );

		// Shipping method ID
		$shipping_method = @array_shift($this->order->get_shipping_methods());
		$this->setShippingMethodId( $shipping_method['method_id'] );

		if( 'local_pickup_plus' == $this->getShippingMethodId() ){
			$this->calcPickUpStoreId();
			$pickup_department_id = Department::getDepartmentIdFromStoreId( $this->getPickUpStoreId() );
			$this->setPickUpDepartmentId( $pickup_department_id );
		}
	}






	private function calcPickUpStoreId(){
		$shipping_methods = $this->getOrder()->get_shipping_methods();
		$first_key = array_key_first( $shipping_methods );
		$temp_shipping_method = $shipping_methods[ $first_key ];
		$pickup_store_id = $temp_shipping_method->get_meta( '_pickup_location_id' );
		if( ! empty( $pickup_store_id ) ){
			$this->setPickUpStoreId( $pickup_store_id );
		} else {
			$this->setPickUpStoreId( 0 );
		}
	}





	public function displayShippingMethod(){

		switch( $this->getShippingMethodId() ){
			case 'local_pickup_plus':
				// $pick_up_department = $this->getPickUpDepartmentId();
				$department = Department::getDepartmentFromDepartmentId( $this->getPickUpDepartmentId() );
				$department_name = @( $department->getName() ) ?: 'Ukendt';
				//$pick_up_department = LocalPickUpHelpers::getDepartmentInfo( 'department_id', $this->getPickUpDepartmentId() );
				return 'Til afhentning i ' . $department_name;
			case 'free_shipping':
			case 'flat_rate':
				return 'Sendes til kunde';
			default:
				return $this->getShippingMethodId();
		}
	}






	/**
	 * @return mixed
	 */
	public function getShippingMethod(){
		return $this->shipping_method;
	}





	/**
	 * @param mixed $shipping_method
	 */
	public function setShippingMethod( $shipping_method ): void{
		$this->shipping_method = $shipping_method;
	}





	/**
	 * @return mixed
	 */
	public function getShippingMethodId(){
		return $this->shipping_method_id;
	}





	/**
	 * @param mixed $shipping_method_id
	 */
	public function setShippingMethodId( $shipping_method_id ): void{
		$this->shipping_method_id = $shipping_method_id;
	}





	/**
	 * @return int
	 */
	public function getPickUpStoreId(): int{
		return $this->pick_up_store_id;
	}





	/**
	 * @param int $pick_up_store_id
	 */
	public function setPickUpStoreId( int $pick_up_store_id ): void{
		$this->pick_up_store_id = $pick_up_store_id;
	}





	/**
	 * @return mixed
	 */
	public function getOrder(){
		return $this->order;
	}





	/**
	 * @param mixed $order
	 */
	public function setOrder( $order ): void{
		$this->order = $order;
	}





	/**
	 * @return array
	 */
	public function getOrderLines(): array{
		return $this->order_lines;
	}





	/**
	 * @param mixed $order_lines
	 */
	public function addOrderLine( $order_line, $order_line_id ): void{
		$this->order_lines[ $order_line_id ] = $order_line;
	}






	/**
	 * @return mixed
	 */
	public function getOrderAcf(){
		return $this->order_acf;
	}





	/**
	 * @param mixed $order_acf
	 */
	public function setOrderAcf( $order_acf ): void{
		$this->order_acf = $order_acf;
	}





	/**
	 * @return mixed
	 */
	public function getOperation(){
		return $this->operation;
	}





	/**
	 * @param mixed $operation
	 */
	public function setOperation( $operation ): void{
		$this->operation = $operation;
	}





	/**
	 * @return mixed
	 */
	public function getOrderId(){
		return $this->order_id;
	}





	/**
	 * @param mixed $order_id
	 */
	public function setOrderId( $order_id ): void{
		$this->order_id = $order_id;
	}





	public function getDateTime( $return_format = 'H:i - d. M' ){
		$original_order = $this->getOrder();
		if( $original_order ){
			$date = $original_order->get_date_created();
			if( !empty( $return_format ) ){
				return $date->format( $return_format );
			} else {
				return $date;
			}
		}
		return '';
	}





	public function displayDateTime(){
		$original_order = $this->getOrder();
		if( $original_order ){
			$datetime = $original_order->get_date_created();
			$date = $datetime->format( 'd. M' );
			$time = $datetime->format( 'H:i' );
			?>
			<p>
				<strong><?php echo $date; ?></strong><br>
				<span><?php echo $time; ?></span>
			</p>
			<?php
		}
	}





	/**
	 * @return mixed
	 */
	public function getResponsibleDepartment(){
		return $this->responsible_department;
	}





	/**
	 * @param mixed $responsible_department
	 */
	public function setResponsibleDepartment( int $responsible_department ): void{
		$this->responsible_department = $responsible_department;
	}





	/**
	 * @return mixed
	 */
	public function getShippedFromDepartment(){
		return $this->shipped_from_department;
	}





	/**
	 * @param mixed $shipped_from_department
	 */
	public function setShippedFromDepartment( int $shipped_from_department ): void{
		$this->shipped_from_department = $shipped_from_department;
	}





	/**
	 * @return int
	 */
	public function getPickUpDepartmentId(): int{
		return $this->pick_up_department_id;
	}





	/**
	 * @param int $pick_up_department_id
	 */
	public function setPickUpDepartmentId( int $pick_up_department_id ): void{
		$this->pick_up_department_id = $pick_up_department_id;
	}





	/**
	 * @return int
	 */
	public function getLastPerformedStep(): int{
		return $this->last_performed_step;
	}





	/**
	 * @param int $last_performed_step
	 */
	public function setLastPerformedStep( int $last_performed_step ): void{
		$this->last_performed_step = $last_performed_step;
	}





	/**
	 * @return string
	 */
	public function getPurAndPartPickingAlgorithm(): string{
		return $this->pur_and_part_picking_algorithm;
	}





	/**
	 * @param string $pur_and_part_picking_algorithm
	 */
	public function setPurAndPartPickingAlgorithm( string $pur_and_part_picking_algorithm ): void{
		$this->pur_and_part_picking_algorithm = $pur_and_part_picking_algorithm;
	}





	/**
	 * @return array
	 */
	public function getStepValidationErrors(): array{
		$automation_flow = $this->getAutomationFlow();
		$step_validation_errors = [];
		foreach( $automation_flow as $step_number => $array ){
			$errors = $array['errors'];
			if( !empty( $errors ) ){
				$imploded_errors = implode( ' || ', $errors );
				$step_validation_errors[ $step_number ] = $imploded_errors;
			}
		}
		return $step_validation_errors;
	}





	public function addStepValidationError( string $step_validation_error, $step_number = null ): void{
		if( is_null( $step_number ) ){
			$step_number = $this->getStepInProgress();
		}
		$this->automation_flow[ $step_number ][ 'errors' ][] = $step_validation_error;
	}





	/**
	 * @return array
	 */
	public function getAutomationProducts(): array{
		$order_lines = $this->getOrderLines();
		$automation_products = [];
		foreach( $order_lines as $order_line ){
			$automation_products = array_merge( $order_line->getAutomationProducts(), $automation_products );
		}
		return $automation_products;
	}




	public function getAutomationPurs(): array{
		$automation_products = $this->getAutomationProducts();
		$automation_purs = [];
		foreach( $automation_products as $automation_product ){
			if( $automation_product->isPur() ){
				$automation_purs[] = $automation_product;
			}
		}
		return $automation_purs;
	}





	/**
	 * @param array $automation_products
	 */
	public function setAutomationProducts( array $automation_products ): void{
		$this->automation_products = $automation_products;
	}






	public function addChosenAccessoryOnAutomationOrder( $full_part_id, int $department_id, array $chosen_accessory_contents ): void{
		throw new Exception( 'addChosenAccessoryOnAutomationOrder shouldnt be used. use addAutomationPart');
		if( !array_key_exists( $full_part_id, $this->automation_products ) ){
			$this->automation_products[ $full_part_id ] = [];
		}

		$this->automation_products[ $full_part_id ][ $department_id ] = $chosen_accessory_contents;
	}





	/**
	 * @return array
	 */
	public function getAutomationFlow(): array{
		return $this->automation_flow;
	}





	/**
	 * @param array $automation_flow
	 */
	public function setAutomationFlow( array $automation_flow ): void{
		$this->automation_flow = $automation_flow;
	}





	/**
	 * @return array
	 */
	public function getInitialAutomationProducts(): array{
		return $this->initial_automation_products;
	}





	/**
	 * @param array $initial_automation_products
	 */
	public function setInitialAutomationProducts( array $initial_automation_products ): void{
		$this->initial_automation_products = $initial_automation_products;
	}





	/**
	 * @return int
	 */
	public function getStepInProgress(): int{
		return $this->step_in_progress;
	}





	/**
	 * @param int $step_in_progress
	 */
	public function setStepInProgress( int $step_in_progress ): void{
		$this->step_in_progress = $step_in_progress;
	}





	/**
	 * @return int
	 */
	public function getFinalInternalDestination(): int{

		// If 0, then attempt a calculation
		if( $this->final_internal_destination === 0 ){

			$shipping_method = $this->getShippingMethodId();
			if( $shipping_method == 'local_pickup_plus' ){
				return $this->getPickUpDepartmentId();
			}
		}
		return $this->final_internal_destination;
	}





	/**
	 * @param int $final_internal_destination
	 */
	public function setFinalInternalDestination( int $final_internal_destination ): void{
		$this->final_internal_destination = $final_internal_destination;
	}





	/**
	 * @return array
	 */
	public function getAddonPartsGrossList(): array{
		return $this->addon_parts_gross_list;
	}





	/**
	 * @param array $addon_parts_gross_list
	 */
	public function setAddonPartsGrossList( array $addon_parts_gross_list ): void{
		$this->addon_parts_gross_list = $addon_parts_gross_list;
	}





	/**
	 * @return bool
	 */
	public function isManualHandlingEnabled(): bool{
		return $this->manual_handling_enabled;
	}





	/**
	 * @param bool $manual_handling_enabled
	 */
	public function setManualHandlingEnabled( bool $manual_handling_enabled ): void{
		$this->manual_handling_enabled = $manual_handling_enabled;
	}





	/**
	 * @return array
	 */
	public function getReasonsForManualHandling(): array{
		return $this->reasons_for_manual_handling;
	}





	/**
	 * @param string $reason_for_manual_handling
	 */
	public function addReasonForManualHandling( string $reason_for_manual_handling ): void{
		$this->reasons_for_manual_handling[] = $reason_for_manual_handling;
	}





	/**
	 * @return bool
	 */
	public function isAddonsHandled(): bool{
		$automation_purs = $this->getAutomationPurs();
		foreach( $automation_purs as $automation_pur ){
			if( ! $automation_pur->isAddonsHandled() ){
				return false;
			}
		}
		return true;
	}





	/**
	 * @return string
	 */
	public function getAddonAlgorithm(): string{
		return $this->addon_algorithm;
	}





	/**
	 * @param string $addon_algorithm
	 */
	public function setAddonAlgorithm( string $addon_algorithm ): void{
		$this->addon_algorithm = $addon_algorithm;
	}





}
?>

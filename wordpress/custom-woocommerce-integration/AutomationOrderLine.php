<?php





class AutomationOrderLine {
	private AutomationOrder $automation_order;
	private $order_line;
	private int $order_line_id;
	private string $type = '';
	private int $quantity = 0;
	private $base_product_id; // Variable products -> parents. Simple -> themselves
	private $sold_product; // Variable products -> themselves. Simple -> themselves
	private int $created_from_model_type_id;
	private int $created_from_model_id;
	private int $created_from_part_id;
	private string $created_from_gms_object = '';
	private array $possible_purs = [];
	private array $possible_parts = [];
	private GmsAccessory $part; // If the sold item is an accessory
	private array $comment = [];
	private array $parsed_meta_data = [];
	private array $automation_products = [];
	private bool $addons_handled = false;





	public function __construct( AutomationOrder $automation_order, $order_line, $order_line_id ){
		$this->setAutomationOrder( $automation_order );
		$this->setOrderLine( $order_line );
		$this->setOrderLineId( $order_line_id );
		$this->setSoldProduct( $order_line->get_product() );
		$this->parseMetaLines();

		if( $this->getSoldProduct() ){ // Is false if product is deleted

			if( 'variation' == $this->getSoldProduct()->get_type() ){
				$this->setType( 'variation' );
				$this->populateInfoFromVariableProduct();
			} else {
				$this->setType( 'simple' );
				$this->populateInfoFromSimpleProduct();
			}
		}
	}





	public function getMissingQuantity(): int {

		$selected_automation_products = $this->getAutomationOrder()->getAutomationProducts();
		if( empty( $selected_automation_products ) ){
			return $this->getQuantity();
		}

		$returned_quantity = $this->getQuantity();
		foreach( $selected_automation_products as $semi_full_pur_part_id => $selected_automation_product ){

			// Not relevant for PUR

			// PARTs
			if( $selected_automation_product->isPart() && $this->getCreatedFromGmsObject() == 'PART' ){
				$parsed_semi_full_part_id = GmsAccessory::parseSemiFullPartId( $semi_full_pur_part_id );
				$automation_order_line_part_id = $this->getCreatedFromPartId();

				if( $parsed_semi_full_part_id['part_id'] == $automation_order_line_part_id ){
					$selected_automation_product_quantity = $selected_automation_product->getQuantity();
					$returned_quantity = $returned_quantity - $selected_automation_product_quantity;
				}
			}
		}
		return $returned_quantity;
	}





	public function validateForcedPurMetaData(): bool {
		$parsed_meta_data = $this->getParsedMetaData();
		if( array_key_exists( '_force-PUR', $parsed_meta_data ) ){

			// Check that its an array
			if( ! is_array( $parsed_meta_data[ '_force-PUR' ] ) ){
				$this->getAutomationOrder()->addStepValidationError( 'There was parsedMetaLines called _force-PUR, but the value wasnt an array. Ehm!' );
				return false;
			}

			// Validate format
			foreach( $parsed_meta_data['_force-PUR'] as $forced_pur_id ){
				$validation_errors = GmsPur::validatePurId( $forced_pur_id );
				if( !empty( $validation_errors ) ){
					$this->getAutomationOrder()->addStepValidationError( 'The forced pur_id was set to: ' . $forced_pur_id . ' - but that is invalidated with the following error(s): ' . DevHelpers::implode_all( ', ', $validation_errors ) );
					return false;
				}
			}

			// Check the quantity
			$qty = $this->getQuantity();
			$forced_pur_qty = count( $parsed_meta_data[ '_force-PUR' ] );
			if( $forced_pur_qty > $qty ){
				$this->getAutomationOrder()->addStepValidationError( 'There are more _force-PUR lines than quantity of the order line.' );
			}
		}

		return true;
	}





	public function validateForcedPartMetaData(): bool {
		$parsed_meta_data = $this->getParsedMetaData();
		if( array_key_exists( '_force-PART', $parsed_meta_data ) ){

			if( ! is_array( $parsed_meta_data[ '_force-PART' ] ) ){
				$this->getAutomationOrder()->addStepValidationError( 'There was parsedMetaLines called _force-PART, but the value wasnt an array. Ehm!' );
				return false;
			}

			// Validate format
			foreach( $parsed_meta_data['_force-PART'] as $forced_part_id ){
				$validation_errors = GmsAccessory::validateFullPartId( $forced_part_id );
				if( !empty( $validation_errors ) ){
					$this->getAutomationOrder()->addStepValidationError( 'The forced part_id was set to: ' . $forced_part_id . ' - but that is invalidated with the following error(s): ' . DevHelpers::implode_all( ', ', $validation_errors ) );
					return false;
				}
			}

			// Validate QTY
			$forced_qty = 0;
			foreach( $parsed_meta_data['_force-PART'] as $forced_part_id ){
				$parsed_part_id = GmsAccessory::parseFullPartId( $forced_part_id );
				$forced_qty += $parsed_part_id[ 'quantity' ];
			}

			if( $forced_qty > $this->getQuantity() ){
				$this->getAutomationOrder()->addStepValidationError( 'The forced part_id had a higher quantity ('.  $forced_qty .') than there was items sold on the order line (' . $this->getQuantity() . ').');
				return false;
			}
		}

		return true;
	}





	/**
	 * Both adds the line to the database,
	 * but also adds the value to parsedMetaData,
	 * ... so no reload of all the order lines are necessary
	 */
	public function addOrderItemMeta( $key, $value ){
		wc_add_order_item_meta( $this->getOrderLineId(), $key, $value );
		$this->addParsedMetaData( $key, $value );
	}




	public function removeOrderItemMeta( $key, $value ){
		wc_delete_order_item_meta( $this->getOrderLineId(), $key, $value );
		$this->removeParsedMetaData( $key, $value );
	}






	public function updateOrderItemMeta( $key, $new_value ){
		wc_update_order_item_meta( $this->getOrderLineId(), $key, $new_value );
		if( array_key_exists( $key, $this->parsed_meta_data ) ){
			if( count( $this->parsed_meta_data[ $key ] ) === 1 ){
				$this->parsed_meta_data[ $key ][0] = $new_value;
			}
			// If more than one value is found, then nothing is done, since something is off.
			// I don't wanna throw an exception on this. :-/
		} else {
			// Doesnt exists
			$this->parsed_meta_data[ $key ] = [ $new_value ];
		}
	}





	public function increaseQuantityForAutomationProduct( $semi_full_part_id ): void {
		$automation_products = $this->getAutomationProducts();
		$chosen_automation_product = null;
		foreach( $automation_products as $automation_product ){
			if( $automation_product->getSemiFullPurPartId() === $semi_full_part_id ){
				$chosen_automation_product = $automation_product;
			}
		}

		if( $chosen_automation_product ){
			$currentQty = $chosen_automation_product->getQuantity();
			$newQty = $currentQty + 1;
			$chosen_automation_product->setQuantity( $newQty );
		}
	}




	public function save(){
		$raw_order_line = $this->getOrderLine();
		$raw_order_line->save();
	}






	public function getPurCondition(){

		if( $this->getCreatedFromGmsObject() === 'PUR' ){
			$parsed_meta_data = $this->getParsedMetaData();
			if( array_key_exists( 'pa_stand', $parsed_meta_data ) ){
				return $parsed_meta_data['pa_stand'][0];
			}
		}

		return false;
	}





	private function getValFromArgs( $args, $key ){

		if( isset( $args[ $key ] ) ){

			$booleans = [
				'is_internal_tnt_readonly',
				'is_tnt_to_customer_readonly'
			];

			if( in_array( $key, $booleans ) ){
				return boolval( $args[ $key ] );
			} else {
				return $args[ $key ];
			}
		}

		return false;
	}





	private function displayShippingStatus( $shipping_status ){
		switch( $shipping_status ){
			case 'ready_to_be_sent':
				echo 'Klar til forsendelse';
				break;
			case 'internal_shipping_required':
				echo 'Intern forsendelse';
				break;
			default:
				echo $shipping_status;
				break;
		}
	}





	public function getDepartmentAndShippingInfo( $statuses_to_include = [] ){
		$automation_products = $this->getAutomationOrder()->getAutomationProducts();
		$department_shipping_info = [];
		if( $this->chosenAutomationProductsAreSaturated() ){

			if( $this->getType() == 'simple' ){
				$full_part_id = 'PART' . $this->getCreatedFromPartId();
				if( array_key_exists( $full_part_id, $automation_products ) ){
					$chosen_parts = $automation_products[ $full_part_id ];
					foreach( $chosen_parts as $department_id => $part_details ){
						$part_shipping_status = @( $part_details['shipping_status'] ) ?: '';

						if( empty( $statuses_to_include ) || in_array( $part_shipping_status, $statuses_to_include ) ){
							if( ! array_key_exists( $department_id, $department_shipping_info ) ){
								$department_shipping_info[ $department_id ] = [];
							}
							$department_shipping_info[ $department_id ][ $full_part_id ] = $part_shipping_status;
						}

					}
				}
			}

			if( $this->getType() == 'variation' ){
				$chosen_purs = $this->getChosenPurs();
				foreach( $chosen_purs as $pur_id => $pur ){
					if( array_key_exists( 'PUR' . $pur_id,  $automation_products ) ){
						$department_id = $automation_products[ 'PUR' . $pur_id ][ 'current_location_id' ];
						$pur_shipping_status = $automation_products[ 'PUR' . $pur_id ][ 'shipping_status' ];

						if( empty( $statuses_to_include ) || in_array( $pur_shipping_status, $statuses_to_include ) ){
							if( ! array_key_exists( $department_id, $department_shipping_info ) ){
								$department_shipping_info[ $department_id ] = [];
							}
							$department_shipping_info[ $department_id ][ 'PUR' . $pur_id ] = $pur_shipping_status;
						}
					}
				}
			}
		}

		return $department_shipping_info;
	}





	private function calcAndSetAutomationProductsFromInitialValues(){
		$initial_automation_products = $this->getAutomationOrder()->getInitialAutomationProducts();

		foreach( $initial_automation_products as $full_pur_part_id => $automation_pur_or_part ){
			$this->addAutomationProduct( $automation_pur_or_part );
		}
	}







	public function chosenAutomationPartsAreSaturated(): bool {
		$quantity = $this->getQuantity();
		$automation_parts = $this->getAutomationParts();
		$automation_parts_quantity = AutomationHelpers::getQuantityFromAutomationParts( $automation_parts );

		return ( $automation_parts_quantity === $quantity );
	}





	public function chosenAutomationPursAreSaturated(): bool {
		$quantity = $this->getQuantity();
		$automation_purs_quantity = count( $this->getAutomationPurs() );
		return ( $automation_purs_quantity === $quantity );
	}






	public function chosenAutomationProductsAreSaturated(): bool{

		if( $this->getCreatedFromGmsObject() == 'PART' ){
			return $this->chosenAutomationPartsAreSaturated();
		}

		if( $this->getCreatedFromGmsObject() == 'PUR' ){
			return $this->chosenAutomationPursAreSaturated();
		}

		return false;
	}







	private function getCostForAllAutomationProducts(){
		$total_cost = 0;

		if( $this->getType() == 'variation' ){
			$automation_purs = $this->getAutomationPurs();
			foreach( $automation_purs as $automation_pur ){
				$total_cost += floatval( $automation_pur->getCostTotalPrice() ) ?: 0;
			}
			return $total_cost;
		} elseif( $this->getType() == 'simple' ){
			$automation_parts = $this->getAutomationParts();
			$an_arbitrary_part = array_values( $automation_parts )[0];
			$single_cost = floatval( $an_arbitrary_part->getCostPrice() ) ?: 0;
			$quantity = AutomationHelpers::getQuantityFromAutomationParts( $automation_parts );
			return $single_cost * $quantity;
		}

		return false;
	}





	public function verifyAndSetCostMetaLine(){
		$cost_meta_line_exists = false;
		$cost_meta_line_needs_updating = false;
		$cost_value = $this->getCostForAllAutomationProducts();

		if( $cost_value === false ){
			$this->getAutomationOrder()->addOrderNote( '_cost blev ikke sat på ordrelinjen med id: ' . $this->getOrderLineId() . '. Muligvis fordi at antallet af valgte PUR ikke passer med antallet af solgte enheder (eller noget i den dur). ' );
			return;
		}

		if( ! $this->metaExists( '_cost', $cost_value ) ){
			$this->addMetaLine( '_cost', $cost_value );
		} else {
			$current_cost_value = $this->getMetaValue( '_cost' );

			if( is_array( $current_cost_value ) && count( $current_cost_value ) > 1 ){
				wc_delete_order_item_meta( $this->getOrderLineId() , '_cost' );
				$this->addMetaLine( '_cost', $cost_value );
			} else {
				// Only one exists
				if( $current_cost_value != $cost_value ){
					$this->updateMetaLine( '_cost', $cost_value );
				}
			}
		}
	}






	private function calcAndSetPartFromPartId( int $part_id ): void {
		$this->setCreatedFromGmsObject( 'PART' );
		$this->setCreatedFromPartId( $part_id );
		$part = GmsAccessory::createFromPartId( $part_id );
		if( $part ){
			$this->setPart( $part );
		}
	}





	private function parseMetaLines(){
		$meta_datas = $this->getOrderLine()->get_meta_data();
		$parsed_meta_data = [];
		if( !empty( $meta_datas ) ){
			foreach( $meta_datas as $meta_data ){
				if( ! array_key_exists( $meta_data->key, $parsed_meta_data ) ){
					$parsed_meta_data[ $meta_data->key ] = [];
				}
				$parsed_meta_data[ $meta_data->key ][] = $meta_data->value;
			}
		}

		$this->setParsedMetaData( $parsed_meta_data );
	}





	public function getTouchedDepartments(): array{
		$touched_departments = [];
		$automation_products = $this->getAutomationProducts();

		foreach( $automation_products as $automation_product ){
			$sold_from_department_id = $automation_product->getSoldFromDepartmentId();
			if( $sold_from_department_id !== 0 ){
				$touched_departments[] = $sold_from_department_id;
			}
		}
		return array_unique( $touched_departments );
	}





	public function displayPurContents( $chosen_purs, $args = [] ){
		throw new Exception( 'displayPurContents is deprecated' );
		$relevant_to_department_id = @( intval( $args['relevant_to_department_id'] ) ) ?: -1;
		$hide_department = ( isset( $args['hide_department'] ) ? boolval( $args['hide_department'] ): true );

		$tnt_to_show = $this->getValFromArgs( $args, 'tnt_to_show' );
		$is_internal_tnt_readonly = $this->getValFromArgs( $args, 'is_internal_tnt_readonly' );
		$is_tnt_to_customer_readonly = $this->getValFromArgs( $args, 'is_tnt_to_customer_readonly' );
		$tnt_placeholder = $this->getValFromArgs( $args, 'tnt_placeholder' );

		$automation_products = $this->getAutomationOrder()->getAutomationProducts();
		if( !empty( $chosen_purs ) ){
			foreach( $chosen_purs as $pur_id => $pur ){
				$full_pur_id = 'PUR' . $pur_id;
				$additional_info = [];
				if( array_key_exists( $full_pur_id, $automation_products ) ){
					$additional_info = $automation_products[ 'PUR' . $pur_id ];
				}
				$sold_from_department_id = @( $additional_info['current_location_id'] ) ?: '';
				$shipping_status = @( $additional_info['shipping_status'] ) ?: '';
				$track_n_trace_internal = @( $additional_info['track_n_trace_internal'] ) ?: '';
				$track_n_trace_to_customer = @( $additional_info['track_n_trace_to_customer'] ) ?: '';
				$order_line_id = @( $additional_info['order_line_id'] ) ?: '';

				if( $relevant_to_department_id == -1 || $relevant_to_department_id == $sold_from_department_id ){

				}
				?>
				<div class="shipping-pur-part-content">
					<div class="shipping-pur-part-line line-1">
						<p>
							<?php $this->displayPurPartId( $full_pur_id ); ?>
						</p>
					</div>
					<!-- /.shipping-pur-part-line -->
					<div class="shipping-pur-part-line line-2">
						<p>
							<?php
							if( ! $hide_department ):
								$department = Department::getDepartmentFromDepartmentId( $sold_from_department_id );
								?>
								<span class="location"><i class="fas fa-map-marker"></i> <?php echo $department['name']; ?></span>
								<span class="separator"></span>
							<?php
							endif;
							?>
							<span class="shipping-status">
								<i class="fas fa-shipping-fast"></i>
								<?php $this->displayShippingStatus( $shipping_status ); ?>
							</span>
						</p>
						<?php
						if( $tnt_to_show == 'internal' ){
							$tnt_args = [
								'type' => 'internal',
								'value' => $track_n_trace_internal,
								'readonly' => $is_internal_tnt_readonly,
								'placeholder' => $tnt_placeholder,
								'order_id' => 'test',
								'pur_part_id' => $full_pur_id,
								'order_line_id' => $order_line_id,
								'department_id_for_sender' => '123123'
							];
							$this->displayTrackAndTraceForm( $tnt_args );
						}

						if( $tnt_to_show == 'to_customer' ){
							$tnt_args = [
								'type' => 'to_customer',
								'value' => $track_n_trace_to_customer,
								'readonly' => $is_tnt_to_customer_readonly,
								'placeholder' => $tnt_placeholder,
								'order_id' => 'test',
								'pur_part_id' => $full_pur_id,
								'order_line_id' => $order_line_id,
								'department_id_for_sender' => '123123'
							];
							$this->displayTrackAndTraceForm( $tnt_args );
						}
						?>
					</div>
				</div>
				<!-- /.shipping-pur-part-content -->
				<?php
			}
		}
	}







	public function verifyAndSetMetalinesForAutomationProducts(){
		$automation_products = $this->getAutomationProducts();
		foreach( $automation_products as $automation_product ){

			if( is_a( $automation_product, 'AutomationPur' ) ){
				$this->verifyAndSetMetaLinesForAutomationPur( $automation_product );
			}

			if( is_a( $automation_product, 'AutomationPart' ) ){
				$this->verifyAndSetMetaLinesForAutomationPart( $automation_product );
			}
		}

		$this->verifyAndSetCostMetaLine();
	}






	public function verifyAndSetMetaLinesForAutomationPur( $automation_product ){

		$full_pur_id = $automation_product->getFullPurPartId();
		$pur_id = str_replace( 'PUR', '', $full_pur_id );
		$imei = $automation_product->getImei();
		$gms_category = strtolower( $automation_product->getGmsCategory() );

		switch( $gms_category ){
			case 'smartphone':
				$this->verifyAndSetMetaLine( 'PUR', $pur_id );
				$this->verifyAndSetMetaLine( 'IMEI', $imei );
				break;
			default:
				$this->verifyAndSetMetaLine( 'PUR', $pur_id );
				break;
		}

		if( !empty( $chosen_gms_pur['location_department_id'] ) ){
			$this->getAutomationOrder()->addChosenPurInfo( 'PUR' . $pur_id, 'current_location_id', $chosen_gms_pur['location_department_id'] );
		}
	}





	public function verifyAndSetMetaLinesForAutomationPart( $automation_product ){
		$full_part_id = $automation_product->getFullPurPartId();
		// $part_id = str_replace( 'PART', '', $full_part_id );
		$this->verifyAndSetMetaLine( 'PART', $full_part_id );
	}







	public function deleteAllGreenMindMetaLines(){
		$order_line_metas = $this->getOrderLine()->get_meta_data();
		$keys_to_delete = AutomationHelpers::getMetaKeysThatShouldBeDeletedOnReset();
		$string_end_keys = [
			'-current-location',
			'-current-location-id',
			'-shipping-status',
			'-handling-status',
			'-quantity'
		];

		if( !empty( $order_line_metas ) ){
			foreach( $order_line_metas as $key => $m ){

				if( in_array( $m->key, $keys_to_delete ) ){
					wc_delete_order_item_meta( $this->getOrderLineId() , $m->key );
				}

				foreach( $string_end_keys as $str_end_key ){

					$length = strlen( $str_end_key );
					$neg_length = ( $length * -1 );
					if( substr( $m->key, $neg_length ) === $str_end_key ){
						wc_delete_order_item_meta( $this->getOrderLineId() , $m->key );
					};
				}
			}
		}
	}






	public function getPossiblePursByDepartment(){
		$possible_purs_by_department = [];
		if( !empty( $this->getPossiblePurs() ) ){
			foreach( $this->getPossiblePurs() as $pur ){
				if( ! array_key_exists( $pur['location_department_id'], $possible_purs_by_department ) ){
					$possible_purs_by_department[ $pur['location_department_id'] ] = [];
				}

				$pur_id = $pur['pur'];
				$possible_purs_by_department[ $pur['location_department_id'] ][ $pur_id ] = $pur;
			}
		}

		$possible_purs_by_department = $this->getAutomationOrder()::sortGmsPursByDepartment( $possible_purs_by_department );
		return $possible_purs_by_department;
	}







	private function verifyAndSetMetaLine( $key_to_check, $value, bool $unique = false ){
		$meta_line_exists = false;

		foreach( $this->getParsedMetaData() as $key => $parsed_meta_datum ){
			if( $key == $key_to_check && $parsed_meta_datum == $value ){
				$meta_line_exists = true;
			}
		}

		if( ! $meta_line_exists ){
			$this->addMetaLine( $key_to_check, $value );
		}
	}





	public function addMetaLine( $key, $value, bool $unique = false ){
		$item = $this->getOrderLine();

		if( is_array( $key ) || is_object( $key ) ){
			throw new Exception( '$key is an array and it needs to be a string, int of bool' );
		}

		if( is_array( $value ) || is_object( $value ) ){
			throw new Exception( '$value is an array and it needs to be a string, int of bool' );
		}

		if( $unique ){
			if( $item->meta_exists( $key ) ){
				wc_update_order_item_meta( $item->get_id(), $key, $value );
			} else {
				wc_add_order_item_meta( $item->get_id(), $key, $value );
			}
		} else {
			if( ! $this->metaExists( $key, $value ) ){
				wc_add_order_item_meta( $item->get_id(), $key, $value );
			}
		}
	}





	public function updateMetaLine( $key, $value ){
		$item = $this->getOrderLine();
		wc_update_order_item_meta( $item->get_id(), $key, $value );
	}





	private function metaExists( $key, $value ): bool {
		$parsed_meta_data = $this->getParsedMetaData();
		if( array_key_exists( $key, $parsed_meta_data ) ){
			if( in_array( $value, $parsed_meta_data[ $key ] ) ){
				return true;
			}
		}

		return false;
	}





	private function getMetaValue( $key_to_get ): array|string|bool {
		$returned = [];
		$parsed_meta_data = $this->getParsedMetaData();
		foreach( $parsed_meta_data as $key => $parsed_meta_datum ){
			if( $key == $key_to_get ){
				$returned[] = $parsed_meta_datum;
			}
		}

		if( count( $returned ) == 0 ){
			return false;
		} elseif( count( $returned ) == 1 ){
			return $returned[0];
		} else {
			return $returned;
		}
	}







	private function populateInfoFromVariableProduct(){
		$order_line = $this->getOrderLine();
		$parent_product = $order_line->get_product_id();
		$created_from = strtoupper( get_post_meta( $parent_product, 'created_from_gms_object', true ) );

		$this->setQuantity( $order_line->get_quantity() );
		$this->setBaseProductId( $parent_product );
		$this->setCreatedFromGmsObject( $created_from );

		$model_type_id = get_post_meta( $parent_product, 'created_from_gms_model_type_id', true );
		$model_id = get_post_meta( $parent_product, 'created_from_gms_model_id', true );

		$this->setCreatedFromModelTypeId( intval( $model_type_id ) );
		$this->setCreatedFromModelId( intval( $model_id ) );
	}




	private function populateInfoFromSimpleProduct(){
		// $this->setAddonsHandled( true ); // No addons needed
		$order_line = $this->getOrderLine();
		$base_product_id = $order_line->get_product_id();
		$created_from = strtoupper( get_post_meta( $base_product_id, 'created_from_gms_object', true ) );

		$this->setQuantity( $order_line->get_quantity() );
		$this->setBaseProductId( $base_product_id );
		$this->setCreatedFromGmsObject( $created_from );

		$part_id = get_post_meta( $base_product_id, 'created_from_part_id', true );
		if( !empty( $part_id ) ){
			$this->calcAndSetPartFromPartId( intval( $part_id ) );
		} else {
			$note = 'created_from_part_id var blank for produktet med ID: ' . $base_product_id . '. Fiks det og prøv at kør kørslen igen.';
			$this->getAutomationOrder()->addOrderNote( $note );
		}
	}





	public function calcPossibleParts(){
		$all_automation_products = $this->getAutomationOrder()->getAutomationProducts();

		if( isset( $this->part ) ){
			$part = $this->getPart();
			if( !empty( $part ) ){
				$part_id = $part->getPartId();
				$dep_and_stock = $part->getStock();
				// unset( $part['stock'] );
				$departments_without_empty_stocks = [];
				foreach( $dep_and_stock as $dep_id => $in_stock ){
					$in_stock = intval( $in_stock );

					if( $in_stock > 0 ){
						// Adjust for already chosen products
						$in_stock =  AutomationHelpers::adjustStockForAlreadyChosenPart( $dep_id, $in_stock, $all_automation_products );

						if( $in_stock > 0 ){
							$cloned_part = clone $part;
							$semi_full_part_id = 'PART' . $part_id . '_' . $dep_id;
							$cloned_part->setSemiFullPartId( $semi_full_part_id );
							$departments_without_empty_stocks[ $semi_full_part_id ] = $cloned_part;
						}
					}
				}
				$this->setPossibleParts( $departments_without_empty_stocks );
			}
		}
}





	public function calcPossiblePurs(){
		$order_line_condition = strtolower( $this->getPurCondition() );
		$automation_purs = $this->getAutomationPurs();
		$quantity = $this->getQuantity();
		if( $quantity !== count( $automation_purs ) ){
			$model_type_id = $this->getCreatedFromModelTypeId();
			$model_id = $this->getCreatedFromModelId();
			if( !empty( $model_type_id ) && !empty( $model_id ) ){
				$sellable_statuses = GmsHelpers::getSellableGmsStatuses();

				$pur_query = new WP_Query( [
					'post_type'      => 'gms_pur',
					'post_status'    => 'publish',
					'posts_per_page' => 999,
					'meta_query' => [
						'relation' => 'AND',
						[
							'key' => 'model_type_id',
							'value' => $model_type_id,
							'compare' => '='
						],
						[
							'key' => 'model_id',
							'value' => $model_id,
							'compare' => '='
						],
						[
							'key' => 'pur_status',
							'value' => $sellable_statuses,
							'compare' => 'IN'
						],
					]
				] );
			}

			if( $pur_query->have_posts() ):
				while( $pur_query->have_posts() ):
					$pur_query->the_post();
					$pur_acf_fields = get_fields( get_the_ID() );
					$pur = array_merge( [
						'title' => get_the_title(),
						'wp_id' => get_the_ID(),
						'permalink' => get_the_permalink( get_the_ID() ),
					], $pur_acf_fields );

					$single_pur_condition = strtolower( $pur[ 'condition' ] );
					$single_pur_condition = str_replace( 'stand', '', $single_pur_condition );
					$single_pur_condition = str_replace( ' ', '', $single_pur_condition );

					if( $single_pur_condition === $order_line_condition ){
						$this->addPossiblePur( $pur );
					}
				endwhile; // while( $pur_query->have_posts() ):
				wp_reset_query();
			endif; // if( $pur_query->have_posts() ):

			if( !empty( $this->possible_purs ) ){
				uasort( $this->possible_purs, function( $a, $b ){
					$a_pur_id = (int) $a['pur'];
					$b_pur_id = (int) $b['pur'];

					return ( $a_pur_id >= $b_pur_id ) ? 1 : -1;
				});
			}
		}
	}







	/**
	 * @return mixed
	 */
	public function getType(){
		return $this->type;
	}





	/**
	 * @param mixed $type
	 */
	public function setType( $type ): void{
		$this->type = $type;
	}





	/**
	 * @return mixed
	 */
	public function getBaseProductId(){
		return $this->base_product_id;
	}





	/**
	 * @param mixed $base_product_id
	 */
	public function setBaseProductId( $base_product_id ): void{
		$this->base_product_id = $base_product_id;
	}





	/**
	 * @return mixed
	 */
	public function getSoldProduct(){
		return $this->sold_product;
	}





	/**
	 * @param mixed $sold_product
	 */
	public function setSoldProduct( $sold_product ): void{
		$this->sold_product = $sold_product;
	}






	/**
	 * @return mixed
	 */
	public function getCreatedFromModelTypeId(){
		return $this->created_from_model_type_id;
	}





	/**
	 * @param mixed $created_from_model_type_id
	 */
	public function setCreatedFromModelTypeId( $created_from_model_type_id ): void{
		$this->created_from_model_type_id = $created_from_model_type_id;
	}





	/**
	 * @return mixed
	 */
	public function getCreatedFromModelId(){
		return $this->created_from_model_id;
	}





	/**
	 * @param mixed $created_from_model_id
	 */
	public function setCreatedFromModelId( $created_from_model_id ): void{
		$this->created_from_model_id = $created_from_model_id;
	}





	/**
	 * @return array
	 */
	public function getPossiblePurs(): array{
		return $this->possible_purs;
	}





	/**
	 * @param array $possible_purs
	 */
	public function addPossiblePur( $pur ): void{
		$pur_id = $pur['pur'];
		$this->possible_purs[ $pur_id ] = $pur;
	}





	private function getSaturatedQuantity(){

		if( $this->getCreatedFromGmsObject() === 'PUR' ){
			$already_saturated_quantity = 0;
			$automation_purs = $this->getAutomationParts();
			foreach( $automation_purs as $automation_pur ){
				$already_saturated_quantity++;
			}
			return $already_saturated_quantity;
		}

		if( $this->getCreatedFromGmsObject() === 'PART' ){
			$already_saturated_quantity = 0;
			$automation_parts = $this->getAutomationParts();
			foreach( $automation_parts as $automation_part ){
				$already_saturated_quantity += $automation_part->getQuantity();
			}
			return $already_saturated_quantity;
		}

		return 0;
	}






	/**
	 * @return mixed
	 */
	public function getOrderLine(){
		return $this->order_line;
	}





	/**
	 * @param mixed $order_line
	 */
	public function setOrderLine( $order_line ): void{
		$this->order_line = $order_line;
	}





	/**
	 * @return mixed
	 */
	public function getAutomationOrder(){
		return $this->automation_order;
	}





	/**
	 * @param mixed $automation_order
	 */
	public function setAutomationOrder( $automation_order ): void{
		$this->automation_order = $automation_order;
	}





	/**
	 * @return array
	 */
	public function getComment(): array{
		return $this->comment;
	}





	/**
	 * @param mixed $comment
	 */
	public function addComment( $comment, $step_number = '' ): void{
		throw new Exception( 'addComment shouldnt be used on AutomationOrderLine. Use: $this->getAutomationOrder()->addOrderNote( $note ) instead . ' );
		$key = '_ADM';
		if( ! empty( $step_number ) ) {
			$key = '_ADM-' . $step_number;
		}
		$this->addMetaLine( $key, $comment, false );
		$this->comment[] = $comment;
	}





	/**
	 * @return mixed
	 */
	public function getQuantity(){
		return $this->quantity;
	}





	/**
	 * @param mixed $quantity
	 */
	public function setQuantity( $quantity ): void{
		$this->quantity = $quantity;
	}






	/**
	 * @return mixed
	 */
	public function getGeneratedMetaData(){
		return $this->generated_meta_data;
	}





	/**
	 * @return mixed
	 */
	public function getOrderLineId(){
		return $this->order_line_id;
	}





	/**
	 * @param mixed $order_line_id
	 */
	public function setOrderLineId( $order_line_id ): void{
		$this->order_line_id = $order_line_id;
	}





	/**
	 * @return array
	 */
	public function getParsedMetaData(): array{
		return $this->parsed_meta_data;
	}




	private function addParsedMetaData( $key, $value ){
		if( ! array_key_exists( $key, $this->parsed_meta_data ) ){
			$this->parsed_meta_data[ $key ] = [];
		}

		$this->parsed_meta_data[ $key ][] = $value;
	}




	private function removeParsedMetaData( $key, $value ){
		if( array_key_exists( $key, $this->parsed_meta_data ) ){
			if( in_array( $value, $this->parsed_meta_data[ $key ] ) ){
				$key_for_value = array_search( $value, $this->parsed_meta_data[ $key ] );
				unset( $this->parsed_meta_data[ $key ][ $key_for_value ] );
			}
		}
	}





	/**
	 * @param array $parsed_meta_data
	 */
	public function setParsedMetaData( array $parsed_meta_data ): void{
		$this->parsed_meta_data = $parsed_meta_data;
	}





	/**
	 * @return string
	 */
	public function getCreatedFromGmsObject(): string{
		return strtoupper( $this->created_from_gms_object );
	}





	/**
	 * @param string $created_from_gms_object
	 */
	public function setCreatedFromGmsObject( string $created_from_gms_object ): void{
		$this->created_from_gms_object = $created_from_gms_object;
	}





	/**
	 * @return int
	 */
	public function getCreatedFromPartId(): int{
		return $this->created_from_part_id;
	}





	/**
	 * @param int $created_from_part_id
	 */
	public function setCreatedFromPartId( int $created_from_part_id ): void{
		$this->created_from_part_id = $created_from_part_id;
	}





	/**
	 * @return array
	 */
	public function getAutomationProducts(): array{
		return $this->automation_products;
	}





	/**
	 *
	 * **Newly selected**
	 * Newly selected is there, because if it's the first time a
	 * PUR or a PART is selected, then it saves that selection to a
	 * database, to make it possible to debug something.
	 *
	 * But when the order is simple reinitialized, then they're created as well,
	 * but shouldn't be saved to the debugging database
	 */
	public function addAutomationPart( $full_part_id, $part, $newly_selected = false, $forced_quantity = false ){

		// Setup stuff
		$already_selected_parts = $this->getAutomationParts();
		$parsed_full_part_id = GmsAccessory::parseFullPartId( $full_part_id );
		$department_id = $parsed_full_part_id[ 'department_id' ];
		$missing_quantity = $this->getMissingQuantity();
		$new_part_semi_full_part_id = $parsed_full_part_id['semi_full_part_id'];
		// $already_selected_parts_quantity = AutomationHelpers::getQuantityFromAutomationParts( $already_selected_parts );

		// Calc quantity to add
		$adjusted_quantity_for_part = $part->getAdjustedStockForDepartment( $department_id, $already_selected_parts );
		$to_be_picked = 0;
		if( is_int( $forced_quantity ) ){
			if( $adjusted_quantity_for_part >= $forced_quantity ){
				$to_be_picked = $forced_quantity;
			} else {
				$this->getAutomationOrder()->addStepValidationError( '$adjusted_quantity_for_part var ikke større eller lig med forced_quantity' );
			}
		} else {
			if( $adjusted_quantity_for_part >= $missing_quantity ){
				$to_be_picked = $missing_quantity;
			} else {
				$to_be_picked = $adjusted_quantity_for_part;
			}
		}

		// See if the part_and_department already is chosen - (and increment the quantity)
		$selected_part_was_dealt_with = false;
		foreach( $already_selected_parts as $selected_part_semi_full_part_id => &$automation_part ){
			if( $selected_part_semi_full_part_id === $new_part_semi_full_part_id ){
				$automation_part->increaseQuantity( $missing_quantity );
				$already_chosen_automation_products[ $selected_part_semi_full_part_id ] = $automation_part;
				$selected_part_was_dealt_with = true;
			}
		}

		if( ! $selected_part_was_dealt_with ){
			$automation_part = new AutomationPart( $full_part_id, $part, $this );
			$automation_part->setQuantity( $to_be_picked );
			$this->addAutomationProduct( $automation_part );
		}

		if( $newly_selected ){
			// Add to custom table
			$department_id = $automation_part->getSoldFromDepartmentId();
			$args = [
				'order_id' => $this->getAutomationOrder()->getOrderId(),
				'full_pur_part_id' => $automation_part->getFullPurPartId(),
				'created_at' => date( 'Y-m-d H:i:s' ),
				'updated_at' => date( 'Y-m-d H:i:s' ),
				'comment' => '',
				'department_id' => $department_id
			];
			$sold_by_webshop_line = new SoldByWebshopLine( $args );
			$sold_by_webshop_line->save();
		}
	}





	/**
	 *
	 * **Newly selected**
	 * Newly selected is there, because if it's the first time a
	 * PUR or a PART is selected, then it saves that selection to a
	 * database, to make it possible to debug something.
	 *
	 * But when the order is simple reinitialized, then they're created as well,
	 * but shouldn't be saved to the debugging database
	 */
	public function addAutomationPur( $full_pur_id, $pur, $newly_selected = false ){
		$automation_pur = new AutomationPur( $full_pur_id, $pur, $this );
		$automation_pur->setQuantity( 1 );
		$this->addAutomationProduct( $automation_pur );

		if( $newly_selected ){

			// Add to custom table
			$department_id = $automation_pur->getSoldFromDepartmentId();
			$args = [
				'order_id' => $this->getAutomationOrder()->getOrderId(),
				'full_pur_part_id' => $automation_pur->getFullPurPartId(),
				'created_at' => date( 'Y-m-d H:i:s' ),
				'updated_at' => date( 'Y-m-d H:i:s' ),
				'comment' => '',
				'department_id' => $department_id
			];
			$sold_by_webshop_line = new SoldByWebshopLine( $args );
			$sold_by_webshop_line->save();

			// Add to single PUR
			if( $automation_pur->isPur() ){
				$wp_id = $automation_pur->getWpId();
				$datetime = new DateTime();
				$datetime_str = $datetime->format( 'Y-m-d H:i:s' );
				update_post_meta( $wp_id, 'sold_on_webshop_timestamp', $datetime_str );
			}
		}
	}






	private function addAutomationProduct( AutomationProduct $automation_product ): void {
		$semi_full_pur_part_id = $automation_product->getSemiFullPurPartId();
		$this->automation_products[ $semi_full_pur_part_id ] = $automation_product;
	}




	public function getAutomationPurs(): array {
		$returned = [];
		$automation_products = $this->getAutomationProducts();
		foreach( $automation_products as $automation_product ){
			if( is_a( $automation_product, 'AutomationPur' ) ){
				$returned[ $automation_product->getFullPurPartId() ] = $automation_product;
			}
		}
		return $returned;
	}




	public function getAutomationParts(): array {
		$returned = [];
		$automation_products = $this->getAutomationProducts();
		foreach( $automation_products as $automation_product ){
			if( is_a( $automation_product, 'AutomationPart' ) ){
				$returned[ $automation_product->getSemiFullPurPartId() ] = $automation_product;
			}
		}
		return $returned;
	}





	/**
	 * @return array
	 */
	public function getPossibleParts(): array{
		return $this->possible_parts;

		// Format:
		// [
		//   PART11098_6 => GmsAccessory,
		//   PART11098_4 => GmsAccessory,
		// ]
		//
		// ... NOTE!! It's semi_full_part_ids as keys,
		// since the quantity isn't determined nor relevant
	}





	/**
	 * @param array $possible_parts
	 */
	public function setPossibleParts( array $possible_parts ): void{
		$this->possible_parts = $possible_parts;
	}





	/**
	 * @return GmsAccessory
	 */
	public function getPart(): GmsAccessory{
		return $this->part;
	}





	/**
	 * @param GmsAccessory $part
	 */
	public function setPart( GmsAccessory $part ): void{
		$this->part = $part;
	}






}
?>

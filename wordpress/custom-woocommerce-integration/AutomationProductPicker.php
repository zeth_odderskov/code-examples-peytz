<?php




class AutomationProductPicker {
	private string $algorithm = '';
	private array $pur_order_lines = [];
	private array $part_order_lines = [];
	private AutomationOrder $automation_order;




	public function __construct( array $pur_order_lines, array $part_order_lines, AutomationOrder $automation_order ){
		$this->setAutomationOrder( $automation_order );
		$this->setPurOrderLines( $pur_order_lines);
		$this->setPartOrderLines( $part_order_lines);

		if( $this->getAutomationOrder()->getShippingMethodId() == 'local_pickup_plus' ){
			$pickup_department_id = $this->getPickupDepartmentId();
			$this->setFinalInternalDestination( $pickup_department_id );
		}
	}




	public function findChosenPursAndParts(){
		$pur_order_lines = $this->getPurOrderLines();
		$part_order_lines = $this->getPartOrderLines();

		// Shipping method is pickup - and all products are picked from that department
		$this->findChosenProductsAbsoluteBestCase( $pur_order_lines, $part_order_lines );

		// All sent from one department
		$pur_order_lines_are_saturated = $this->areOrderLinesSaturated( $pur_order_lines );
		$part_order_lines_are_saturated = $this->areOrderLinesSaturated( $part_order_lines );
		if( ! $pur_order_lines_are_saturated || ! $part_order_lines_are_saturated ){
			$this->findChosenPurAndPartBestCase( $pur_order_lines, $part_order_lines );
		}

		// Just find the stuff whereever it is
		$pur_order_lines_are_saturated = $this->areOrderLinesSaturated( $pur_order_lines );
		$part_order_lines_are_saturated = $this->areOrderLinesSaturated( $part_order_lines );
		if( ! $pur_order_lines_are_saturated || ! $part_order_lines_are_saturated ){
			$this->findChosenPurAndPartWorstCase( $pur_order_lines, $part_order_lines );
		}

	}




	private function getChosenPursForOrderLine( $pur_list, $quantity ): array {
		$chosen_pur = [];

		if( !empty( $pur_list ) ){
			foreach( $pur_list as $pur_id => $pur ){
				if( count( $chosen_pur ) < $quantity ){
					$chosen_pur[ $pur_id ] = $pur;
				}
			}
		}

		return $chosen_pur;
	}





	private function getChosenPartsForOrderLine( $part_list, $quantity ): array {
		$chosen_part = [];
		$needs_to_be_picked = $quantity;

		if( !empty( $part_list ) ){
			foreach( $part_list as $part_id => $part ){
				if( $needs_to_be_picked > 0 ){
					$semi_full_part_id = $part->getSemiFullPartId();
					$dep_id = GmsHelpers::getDepIdFromFullPartId( $semi_full_part_id );
					$in_stock_for_dep = $part->getStockForDepartment( $dep_id );
					if( $in_stock_for_dep >= $needs_to_be_picked ){
						$part->setToBePicked( $needs_to_be_picked );
						$chosen_part[ $part_id ] = $part;
						$needs_to_be_picked = 0;
					} else {
						$part->setToBePicked( $in_stock_for_dep );
						$chosen_part[ $part_id ] = $part;
						$needs_to_be_picked = $needs_to_be_picked - $in_stock_for_dep;
					}
				}
			}
		}

		return $chosen_part;
	}





	private function findDepAndPursForOrderLinesAbsoluteBestCase( $pur_order_lines, $pick_up_department_id ): array {
		$order_line_dep_and_pur = [];
		foreach( $pur_order_lines as $order_line_id => $pur_order_line ){
			$order_line_dep_and_pur[ $order_line_id ] = [];
			$sold_quantity_on_order_line = $pur_order_line->getQuantity();
			$dep_and_possible_pur = $pur_order_line->getPossiblePursByDepartment();
			if(
				array_key_exists( $pick_up_department_id, $dep_and_possible_pur ) && // is in dep
				count( $dep_and_possible_pur[ $pick_up_department_id ] ) >= $sold_quantity_on_order_line // has enough stock
			){
				$order_line_dep_and_pur[ $order_line_id ] = [
					$pick_up_department_id => $dep_and_possible_pur[ $pick_up_department_id ]
				];
			}
		}

		// Returns something like:
		//			373 => [
		//				[
		//					3 => [
		//						13965 => [ ... Entire PUR ]
		//					]
		//				]
		//			],
		//			374 => [
		//        // Empty if non is found, for that dep
		//			],

		return $order_line_dep_and_pur;
	}





	private function findDepAndPartsForOrderLinesAbsoluteBestCase( $part_order_lines, $pick_up_department_id ): array {
		$order_line_dep_and_part = [];
		foreach( $part_order_lines as $order_line_id => $part_order_line ){
			$order_line_dep_and_part[ $order_line_id ] = [];
			$sold_quantity_on_order_line = $part_order_line->getQuantity();
			$possible_parts = $part_order_line->getPossibleParts();
			foreach( $possible_parts as $part_id => $possible_part ){ // $part_id ex.: 11098_6
				$dep_id = GmsHelpers::getDepIdFromFullPartId( $part_id );
				//$in_stock_for_department = intval( $possible_part['in_stock'] );
				$in_stock_for_dep = $possible_part->getStockForDepartment( $dep_id );
				if(
					$dep_id === $pick_up_department_id &&
					$in_stock_for_dep >= $sold_quantity_on_order_line
				){
					$order_line_dep_and_part[ $order_line_id ] = [
						$dep_id => [
							$part_id => $possible_part
						]
					];
				}
			}
		}

		// Returns something like:
		//			375 => [
		//				[
		//					3 => [
		//						11098_6 => [ ... Entire Part ]
		//					]
		//				]
		//			],
		//			376 => [
		//        // Empty if non is found, for that dep
		//			],

		return $order_line_dep_and_part;
	}






	private function findChosenProductsAbsoluteBestCase( $pur_order_lines, $part_order_lines ){
		$shipping_method_id = $this->getAutomationOrder()->getShippingMethodId();
		if( $shipping_method_id == 'local_pickup_plus' ){
			$pick_up_department_id = $this->getAutomationOrder()->getPickUpDepartmentId();

			$order_line_dep_and_purs = $this->findDepAndPursForOrderLinesAbsoluteBestCase( $pur_order_lines, $pick_up_department_id );
			$order_line_dep_and_parts = $this->findDepAndPartsForOrderLinesAbsoluteBestCase( $part_order_lines, $pick_up_department_id );


			// Check that all PUR-lines are good enough
			$pur_all_in_pick_up_department = true;
			foreach( $order_line_dep_and_purs as $order_line_id => $dep_and_pur ){
				if( empty( $dep_and_pur ) ){
					$pur_all_in_pick_up_department = false;
				}
			}

			// Check that all Part-lines are good enough
			$parts_all_in_pick_up_department = true;
			foreach( $order_line_dep_and_parts as $order_line_id => $dep_and_part ){
				if( empty( $dep_and_part ) ){
					$parts_all_in_pick_up_department = false;
				}
			}

			// All products are in the pick-up department!!
			if( $pur_all_in_pick_up_department && $parts_all_in_pick_up_department ){
				$this->setAlgorithm( 'absolute-best-case' );

				// Start choosing PUR
				foreach( $pur_order_lines as $order_line_id => $pur_order_line ){
					if( isset( $order_line_dep_and_purs[ $order_line_id ][ $pick_up_department_id ] ) ){
						$pur_list = $order_line_dep_and_purs[ $order_line_id ][ $pick_up_department_id ];
						foreach( $pur_list as $pur_id => $pur_details ){
							if( ! $pur_order_line->chosenAutomationPursAreSaturated() ){
								$gms_pur = GmsPur::createFromPurId( $pur_id );
								$full_pur_id = 'PUR' . $pur_id;
								$pur_order_line->addAutomationPur( $full_pur_id, $gms_pur, true );
							}
						}
					}
				}

				// Start choosing Parts
				foreach( $part_order_lines as $order_line_id => $part_order_line ){
					if( isset( $order_line_dep_and_parts[ $order_line_id ][ $pick_up_department_id ] ) ){
						$part_list = $order_line_dep_and_parts[ $order_line_id ][ $pick_up_department_id ];
						foreach( $part_list as $semi_full_part_id => $part ){
							if( ! $part_order_line->chosenAutomationPartsAreSaturated() ){
								$missing_quantity_sold_on_order_line = $part_order_line->getMissingQuantity();
								$part->setToBePicked( $missing_quantity_sold_on_order_line );
								$full_part_id = $semi_full_part_id . '#' . $missing_quantity_sold_on_order_line;
								$newly_selected = true;
								$part_order_line->addAutomationPart( $full_part_id, $part, $newly_selected );
							}
						}
					}
				}

			}
		}
	}




	private function findDepAndPurBestCase( $pur_order_lines ){
		$gross_list = [];

		// Loop order lines
		foreach( $pur_order_lines as $order_line_id => $pur_order_line ){

			// If products not saturated
			if( ! $pur_order_line->chosenAutomationPursAreSaturated() ){
				$dep_with_enough_stock = [];
				$quantity_sold = $pur_order_line->getQuantity();
				$dep_and_possible_pur = $pur_order_line->getPossiblePursByDepartment();

				// Loop possible PUR
				foreach( $dep_and_possible_pur as $dep_id => $possible_pur ){
					if( count( $possible_pur ) >= $quantity_sold ){
						$dep_with_enough_stock[ $dep_id ] = $possible_pur;
					}
				}

				// Add to gross_list
				$gross_list[ $order_line_id ] = $dep_with_enough_stock;
			}
		}

		// Get shortlist from gross list
		$shortlist = $this->getPurShortlistBestCase( $gross_list );
		return $shortlist;
	}





	private function findDepAndPartsBestCase( $part_order_lines ){

		// Find departments with accessories in stock
		$gross_list = [];
		foreach( $part_order_lines as $order_line_id => $part_order_line ){
			$dep_with_enough_stock = [];
			//$quantity_sold = $part_order_line->getQuantity();
			$missing_quantity = $part_order_line->getMissingQuantity();
			$possible_parts = $part_order_line->getPossibleParts();
			foreach( $possible_parts as $part_id => $part ){

				$dep_id = GmsHelpers::getDepIdFromFullPartId( $part_id );
				$in_stock = $part->getStockForDepartment( $dep_id );
				if( $in_stock >= $missing_quantity ){
					if( ! array_key_exists( $dep_id, $dep_with_enough_stock ) ){
						$dep_with_enough_stock[ $dep_id ] = [];
					}
					$dep_with_enough_stock[ $dep_id ][ $part_id ] =  $part;
				}
			}
			$gross_list[ $order_line_id ] = $dep_with_enough_stock;
		}

		$shortlist = $this->getPartShortlistBestCase( $gross_list );
		return $shortlist;
	}




	private function findPossiblePartsByOrderLineWorstCase( $part_order_lines ){

		// Find departments with accessories in stock
		$gross_list = [];
		foreach( $part_order_lines as $order_line_id => $part_order_line ){
			$possible_parts = $part_order_line->getPossibleParts();
			$gross_list[ $order_line_id ] = $possible_parts;
		}

		return $gross_list;
	}






	private function findPossiblePursByOrderLineWorstCase( $pur_order_lines ){

		$gross_list = [];
		foreach( $pur_order_lines as $order_line_id => $pur_order_line ){

			if( ! $pur_order_line->chosenAutomationPursAreSaturated() ){
				$dep_and_possible_pur = $pur_order_line->getPossiblePurs();
				$gross_list[ $order_line_id ] = $dep_and_possible_pur;
			}
		}

		return $gross_list;
	}





	private function getPurShortlistBestCase( $gross_list ): array {
		$two_products_didnt_have_any_common_departments = false;
		$shortlist = [];

		// Loop gross list
		foreach( $gross_list as $order_line_id => $dep_and_pur ){

			// Flag, if two departments aren't in the list
			if( ! $two_products_didnt_have_any_common_departments ){

				if( empty( $shortlist ) ){

					// First iteration
					$shortlist = $dep_and_pur;

				} else {

					// Loop gross list and add new entries
					// BUT ONLY IF THE DEPARTMENT IS ALREADY IN THE SHORTLIST
					foreach( $dep_and_pur as $dep_id => $new_entry ){
						if( array_key_exists( $dep_id, $shortlist ) ){
							$shortlist[ $dep_id ] = $shortlist[ $dep_id ] + $new_entry;
						}
					}

					// Loop shortlist, ensuring  Remove departments that wasnt there
					foreach( $shortlist as $dep_id => $shortlist_entry ){
						if( ! array_key_exists( $dep_id, $dep_and_pur ) ){
							unset( $shortlist[ $dep_id ] );
						}
					}

					// Two departments had none in common. Raise flag. Bail out, people!
					if( empty( $shortlist ) ){
						$two_products_didnt_have_any_common_departments = true;
					}

				}
			}
		}

		// Returns something like:
		//		[
		//			16 => [
		//				13836 => [ ... Entire PUR ],
		//				13966 => [ ... Entire PUR ],
		//			],
		//			3 => [
		//				13965 => [ ... Entire PUR ]
		//			]
		//		]

		return $shortlist;
	}





	private function getPartShortlistBestCase( $part_gross_list ): array {
		$two_products_didnt_have_any_common_departments = false;
		$shortlist = [];

		foreach( $part_gross_list as $order_line_id => $dep_and_parts ){

			if( ! $two_products_didnt_have_any_common_departments ){
				if( empty( $shortlist ) ){
					$shortlist = $dep_and_parts;
				} else {

					// Add new entries
					foreach( $dep_and_parts as $dep_id => $new_entry ){
						if( array_key_exists( $dep_id, $shortlist ) ){
							$shortlist[ $dep_id ] = $shortlist[ $dep_id ] + $new_entry;
						}
					}

					// Remove departments that wasnt there
					foreach( $shortlist as $dep_id => $shortlist_entry ){
						if( ! array_key_exists( $dep_id, $dep_and_parts ) ){
							unset( $shortlist[ $dep_id ] );
						}
					}

					// Two departments had none in common. Raise flag.
					if( empty( $shortlist ) ){
						$two_products_didnt_have_any_common_departments = true;
					}

				}
			}
		}

		// Returns something like:
		//		[
		//			6 => [
		//				11098_6 => [ ... Entire Part ],
		//			],
		//			3 => [
		//				11098_3 => [ ... Entire Part ]
		//			]
		//		]

		return $shortlist;
	}





	private function chooseDepBestCase( $dep_and_pur, $dep_and_parts ){

		// Calculate potential departments
		$potential_deps = [];

		if( !empty( $dep_and_pur ) && !empty( $dep_and_parts ) ){
			foreach( $dep_and_pur as $pur_department_id => $pur_list ){
				foreach( $dep_and_parts as $part_department_id => $part_list ){
					if( $pur_department_id == $part_department_id ){
						$potential_deps[] = $pur_department_id;
					}
				}
			}
		} elseif( !empty( $dep_and_parts ) ) {
			foreach( $dep_and_parts as $part_department_id => $part_list ){
				$potential_deps[] = $part_department_id;
			}
		} elseif( !empty( $dep_and_pur ) ) {
			foreach( $dep_and_pur as $pur_department_id => $part_list ){
				$potential_deps[] = $pur_department_id;
			}
		}

		if( count( $potential_deps ) == 0 ){
			return false;
		} elseif( count( $potential_deps ) == 1 ){
			return $potential_deps[0];
		} else {

			// More than one department has all products. Oh shit! Let's go, baby!!

			// Order contains PUR
			if( ! empty( $dep_and_pur ) ){
				$oldest_pur_id = 999999999999;
				$department_with_oldest_pur = false;

				foreach( $dep_and_pur as $department_id => $pur_list ){

					if( in_array( $department_id, $potential_deps ) ){
						foreach( $pur_list as $pur_id => $pur_details ){
							$pur_id = intval( $pur_id );
							if( $pur_id < $oldest_pur_id ){
								$oldest_pur_id = $pur_id;
								$department_with_oldest_pur = $department_id;
							}
						}
					}
				}

				return $department_with_oldest_pur;
			}

			// No Pur
			// ... Find department with highest stock number
			$chosen_automation_products = $this->getAutomationOrder()->getAutomationProducts();
			if( !empty( $dep_and_parts ) ){

				$highest_stock_count = 0;
				$department_id_with_highest_stock_count = false;

				foreach( $dep_and_parts as $department_id => $parts ){

					if( in_array( $department_id, $potential_deps ) ){
						foreach( $parts as $part_id => $part ){

							$in_stock = $part->getAdjustedStockForDepartment( $department_id, $chosen_automation_products );
							if( $in_stock > $highest_stock_count ){
								$highest_stock_count = $in_stock;
								$department_id_with_highest_stock_count = $department_id;
							}
						}
					}
				}

				return $department_id_with_highest_stock_count;
			}

			DevHelpers::sendMail( 'Unable to find department', 'This shouldnt ever happen... Empty pur_order_lines and empty part_order_lines. WC-Order-id: ' . $this->getAutomationOrder()->getOrderId() , 'dev'  );
		}

	}





	// All sent from one department
	private function findChosenPurAndPartBestCase( $pur_order_lines, $part_order_lines ){

		// Find departments, with enough in stock
		$should_best_case_attempt_to_run = true;
		$dep_and_pur = $this->findDepAndPurBestCase( $pur_order_lines );
		if( !empty( $pur_order_lines ) && empty( $dep_and_pur ) ){
			$should_best_case_attempt_to_run = false;
		}

		$dep_and_parts = $this->findDepAndPartsBestCase( $part_order_lines );
		if( !empty( $part_order_lines ) && empty( $dep_and_parts ) ){
			$should_best_case_attempt_to_run = false;
		}

		if( $should_best_case_attempt_to_run ){

			// Choose department
			$chosen_department = $this->chooseDepBestCase( $dep_and_pur, $dep_and_parts );
			if( !empty( $chosen_department ) ){

				// Set AutomationProducts and stuff
				$this->setAlgorithm( 'best-case' );
				if( $this->isShipping() ){
					$this->setFinalInternalDestination( $chosen_department );
				}

				$shortlisted_purs = ( !empty( $pur_order_lines ) ) ? $dep_and_pur[ $chosen_department ] : [];
				$shortlisted_parts = ( !empty( $part_order_lines ) ) ? $dep_and_parts[ $chosen_department ] : [];

				// Saturate PUR Order lines
				foreach( $pur_order_lines as $order_line_id => $pur_order_line ){
					foreach( $pur_order_line->getPossiblePurs() as $pur_id => $possible_pur ){
						if(
							array_key_exists( $pur_id, $shortlisted_purs ) &&
							! $pur_order_line->chosenAutomationPursAreSaturated()
						){
							$full_pur_id = 'PUR' . $pur_id;
							$gms_pur = GmsPur::createFromPurId( $pur_id );
							$pur_order_line->addAutomationPur( $full_pur_id, $gms_pur, true );
						}
					}
				}

				// Saturate PART Order lines
				foreach( $part_order_lines as $order_line_id => $part_order_line ){
					foreach( $part_order_line->getPossibleParts() as $part_id => $possible_parts ){

						// DevHelpers::devThrowError();
						if( array_key_exists( $part_id, $shortlisted_parts ) ){
							$part = $shortlisted_parts[ $part_id ];
							$in_stock = $part->getStock();
							//$part_quantity = @( $shortlisted_parts[ $part_id ][ 'in_stock' ] ) ?: 0;
							$quantity_to_be_sold = $part_order_line->getQuantity();
							if(
								$in_stock >= $quantity_to_be_sold &&
								! $part_order_line->chosenAutomationPartsAreSaturated()
							){
								$full_part_id = $part_id . '#' . $quantity_to_be_sold;
								$part = $shortlisted_parts[ $part_id ];
								$part_order_line->addAutomationPart( $full_part_id, $part, true );
							}
						}
					}
				}
			}
		}

	}






	private function findChosenPurAndPartWorstCase( $pur_order_lines, $part_order_lines ){
		$this->setAlgorithm( 'worst-case' );

		// Find departments, with enough in stock
		$possible_pur_by_order_lines = $this->findPossiblePursByOrderLineWorstCase( $pur_order_lines );
		$possible_parts_by_order_lines = $this->findPossiblePartsByOrderLineWorstCase( $part_order_lines );

		if( $this->isShipping() ){
			$this->setFinalInternalDestination( 16 );
		}

		// Saturate PUR Order lines
		foreach( $pur_order_lines as $order_line_id => $pur_order_line ){

			if( ! $pur_order_line->chosenAutomationPursAreSaturated() ){
				$pur_quantity               = $pur_order_line->getQuantity();
				$dep_and_pur_for_order_line = $possible_pur_by_order_lines[ $order_line_id ];
				$chosen_purs                 = $this->getChosenPursForOrderLine( $dep_and_pur_for_order_line, $pur_quantity );

				if( count( $chosen_purs ) == $pur_quantity ){

					foreach( $chosen_purs as $chosen_gms_pur ){
						$full_pur_id = 'PUR' . $chosen_gms_pur[ 'pur' ];
						$gms_pur = GmsPur::createFromPurId( $chosen_gms_pur['pur'] );
						$pur_order_line->addAutomationPur( $full_pur_id, $gms_pur, true );
					}

				} else {
					$this->getAutomationOrder()->addStepValidationError( 'Der var ikke nok PUR på lager til ordrelinie: ' . $order_line_id );

					// Sending mail
					$order_id   = $this->getAutomationOrder()->getOrderId();
					$subject    = 'Ikke nok pur til ordre: ' . $order_id;
					$message    = 'I ordre-ID: ' . $order_id . ' link: ' . get_site_url() . '/wp-admin/post.php?post=' . $order_id . '&action=edit mangler der nok PUR på lager. Ordren er IKKE sendt til GMS og skal håndteres manuelt herfra. Det er ordrelinie: ' . $order_line_id . ' det drejer sig om.';
					DevHelpers::sendMail( $subject, $message, 'wc-error' );
				}
			}
		}

		// Saturate PART Order lines
		foreach( $part_order_lines as $order_line_id => $part_order_line ){

			if( ! $part_order_line->chosenAutomationPartsAreSaturated() ){
				$possible_parts_for_order_line = $possible_parts_by_order_lines[ $order_line_id ];
				$possible_parts_for_order_line = $this->sortPossiblePartsWorstCase( $possible_parts_for_order_line );
				$missing_part_quantity = $part_order_line->getMissingQuantity();
				$chosen_parts = $this->getChosenPartsForOrderLine( $possible_parts_for_order_line, $missing_part_quantity );

				$total_to_be_picked = 0;
				foreach( $chosen_parts as $part_id => $chosen_part ){
					$total_to_be_picked += $chosen_part->getToBePicked();
				}

				if( $total_to_be_picked == $missing_part_quantity ){

					foreach( $chosen_parts as $chosen_part__semi_full_part_id => $part ){
						$to_be_picked = $part->getToBePicked();
						$full_part_id = $chosen_part__semi_full_part_id . '#' . $to_be_picked;
						$part_order_line->addAutomationPart( $full_part_id, $part, true, $to_be_picked );
					}

				} elseif( $total_to_be_picked > $missing_part_quantity ) {
					$this->getAutomationOrder()->addStepValidationError( 'Der blev valgt for mange varer til ordrelinien: ' . $order_line_id );

					// Sending mail
					$order_id = $this->getAutomationOrder()->getOrderId();
					$subject = 'For mange varer valgt til ordre: ' . $order_id;
					$message = 'I ordre-ID: ' . $order_id . ' link: ' . get_site_url() . '/wp-admin/post.php?post='. $order_id .'&action=edit blev der valgt for mange varer. Ordren er IKKE sendt til GMS. Det er ordrelinie: ' . $order_line_id . ' det drejer sig om.';
					DevHelpers::sendMail( $subject, $message, 'wc-error' );
				} else {
					$this->getAutomationOrder()->addStepValidationError( 'Der var ikke nok tilbehør til salget på ordrelinien: ' . $order_line_id );

					// Sending mail
					$order_id = $this->getAutomationOrder()->getOrderId();
					$subject = 'Ikke nok tilbehør til modtaget ordre: ' . $order_id;
					$message = 'I ordre-ID: ' . $order_id . ' link: ' . get_site_url() . '/wp-admin/post.php?post='. $order_id .'&action=edit mangler der nok varer på lager. Ordren er IKKE sendt til GMS. Det er ordrelinie: ' . $order_line_id . ' det drejer sig om.';
					DevHelpers::sendMail( $subject, $message, 'wc-error' );
				}
			}
		}
	}





	private function sortPossiblePartsWorstCase( $possible_parts_for_order_line ){

		// Hovedlager først
		// Derefter dem med mest på lager
		uasort( $possible_parts_for_order_line, function( $a, $b ){
			$a_semi_full_part_id = $a->getSemiFullPartid();
			$a_department_id = GmsHelpers::getDepIdFromFullPartId( $a_semi_full_part_id);

			$b_semi_full_part_id = $b->getSemiFullPartid();
			$b_department_id = GmsHelpers::getDepIdFromFullPartId( $b_semi_full_part_id);

			$a_in_stock = $a->getStockForDepartment( $a_department_id );
			$b_in_stock = $b->getStockForDepartment( $b_department_id );

			if( $a_department_id == 16 ){
				return -2;
			} elseif ( $b_department_id == 16){
				return 2;
			} else {
				if( $a_in_stock > $b_in_stock ){
					return -1;
				} else {
					return 1;
				}
			}
		});

		return $possible_parts_for_order_line;
	}







	private function areOrderLinesSaturated( array $order_line_array ){
		$lines_are_saturated = true;
		foreach( $order_line_array as $order_line_id => $order_line ){
			if( ! $order_line->chosenAutomationProductsAreSaturated() ){
				$lines_are_saturated = false;
			}
		}
		return $lines_are_saturated;
	}





	/**
	 * @return string
	 */
	public function getAlgorithm(): string{
		return $this->algorithm;
	}





	/**
	 * @param string $algorithm
	 */
	public function setAlgorithm( string $algorithm ): void{
		$this->getAutomationOrder()->setPurAndPartPickingAlgorithm( $algorithm );
		$this->algorithm = $algorithm;
	}





	/**
	 * @return array
	 */
	public function getPurOrderLines(): array{
		return $this->pur_order_lines;
	}





	/**
	 * @param array $pur_order_lines
	 */
	public function setPurOrderLines( array $pur_order_lines ): void{
		$this->pur_order_lines = $pur_order_lines;
	}





	/**
	 * @return array
	 */
	public function getPartOrderLines(): array{
		return $this->part_order_lines;
	}





	/**
	 * @param array $part_order_lines
	 */
	public function setPartOrderLines( array $part_order_lines ): void{
		$this->part_order_lines = $part_order_lines;
	}





	/**
	 * @return AutomationOrder
	 */
	public function getAutomationOrder(): AutomationOrder{
		return $this->automation_order;
	}





	/**
	 * @param AutomationOrder $automation_order
	 */
	public function setAutomationOrder( AutomationOrder $automation_order ): void{
		$this->automation_order = $automation_order;
	}




	private function getPickupDepartmentId(): int {
		return $this->automation_order->getPickUpDepartmentId();
	}





	private function getShippingMethodId(): string {
		return $this->automation_order->getShippingMethodId();
	}




	private function getFinalInternalDestination(): int {
		return $this->automation_order->getFinalInternalDestination();
	}




	private function setFinalInternalDestination( int $department_id ): void {
		$this->automation_order->setFinalInternalDestination( $department_id );
	}




	private function isPickup(): bool {
		return $this->getAutomationOrder()->isPickup();
	}





	private function isShipping(): bool {
		return $this->getAutomationOrder()->isShipping();
	}





}
?>

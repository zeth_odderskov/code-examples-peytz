<?php




abstract class AutomationProduct {
	public AutomationOrderLine $automation_order_line;
	public int $order_line_id;
	public string $full_pur_part_id;
	public int $current_location_id;
	public int $final_internal_destination;
	public string $shipping_status;
	public int $sold_from_department_id = 0;
	public bool $marked_as_sold_in_gms;
	public int $quantity;




	public function __construct( string $full_pur_part_id, AutomationOrderLine $order_line ){
		$this->setFullPurPartId( $full_pur_part_id );
		$this->setAutomationOrderLine( $order_line );
		$this->setOrderLineIdFromAutomationOrderLine( $order_line );
		$this->calcAndSetSoldFromDepartment();
	}





	public function getSemiFullPurPartId(){
		if( $this->isPur() ){
			return $this->getFullPurPartId();
		}

		if( $this->isPart() ){
			$full_part_id = $this->getFullPurPartId();
			$exploded_full_part_id = explode( '#', $full_part_id );
			return $exploded_full_part_id[0];
		}
	}





	public function isPur(){
		$first_three = substr( $this->getFullPurPartId(), 0, 3 );
		return ( $first_three === 'PUR' );
	}





	public function isPart(){
		$first_four = substr( $this->getFullPurPartId(), 0, 4 );
		return ( $first_four === 'PART' );
	}






	public function setOrderLineIdFromAutomationOrderLine( AutomationOrderLine $automation_order_line ){
		$order_line_id = $automation_order_line->getOrderLineId();
		$this->setOrderLineId( $order_line_id );
	}





	public function toArray(){

		if( $this->isPur() ){
			$return_val = $this;
			unset( $return_val->automation_order_line );
			$return_val = (array) $return_val;
			$return_val[ 'gms_pur' ] = (array) $this->getGmsPur();
			return $return_val;
		}

		if( $this->isPart() ){
			$return_val = $this;
			unset( $return_val->automation_order_line );
			$return_val = (array) $return_val;
			$return_val[ 'gms_accessory' ] = (array) $this->getGmsAccessory();
			return $return_val;
		}
	}






	private function calcAndSetSoldFromDepartment(){

		if( is_a( $this, 'AutomationPur' ) ){
			$sold_from_department_id = intval( $this->getLocationDepartmentId() );
			$this->setSoldFromDepartmentId( $sold_from_department_id );
		}

		if( is_a( $this, 'AutomationPart' ) ){
			$full_part_id = $this->getFullPurPartId();
			$exploded_full_part_id = explode( '_', $full_part_id );
			if( count( $exploded_full_part_id ) == 2 ){
				$sold_from_department_id = intval( $exploded_full_part_id[1] );
				$this->setSoldFromDepartmentId( $sold_from_department_id );
			}
		}
	}






	public function updateInfo( $key_to_update, $value ){
		if( $key_to_update == 'shipping_status' ){
			$this->setShippingStatus( $value );
		} else {
			throw new Exception( 'updateInfo - key_to_update was not recognized' );
		}
	}





	/**
	 * @return string
	 */
	public function getFullPurPartId(): string{
		return $this->full_pur_part_id;
	}





	/**
	 * @param string $full_pur_part_id
	 */
	public function setFullPurPartId( string $full_pur_part_id ): void{
		$this->full_pur_part_id = $full_pur_part_id;
	}






	/**
	 * @return int
	 */
	public function getCurrentLocationId(): int{
		return $this->current_location_id;
	}





	/**
	 * @param int $current_location_id
	 */
	public function setCurrentLocationId( int $current_location_id ): void{
		$this->current_location_id = $current_location_id;
	}





	/**
	 * @return int
	 */
	public function getFinalInternalDestination(): int{
		return $this->final_internal_destination;
	}





	/**
	 * @param int $final_internal_destination
	 */
	public function setFinalInternalDestination( int $final_internal_destination ): void{
		$this->final_internal_destination = $final_internal_destination;
	}





	/**
	 * @return string
	 */
	public function getShippingStatus(): string{
		return $this->shipping_status;
	}





	/**
	 * @param string $shipping_status
	 */
	public function setShippingStatus( string $shipping_status ): void{
		$this->shipping_status = $shipping_status;
	}





	/**
	 * @return int
	 */
	public function getOrderLineId(): int{
		return $this->order_line_id;
	}





	/**
	 * @param int $order_line_id
	 */
	public function setOrderLineId( int $order_line_id ): void{
		$this->order_line_id = $order_line_id;
	}




	public function getAutomationOrder(): AutomationOrder {
		$automation_order_line = $this->getAutomationOrderLine();
		return $automation_order_line->getAutomationOrder();
	}





	/**
	 * @return AutomationOrderLine
	 */
	public function getAutomationOrderLine(): AutomationOrderLine{
		return $this->automation_order_line;
	}





	/**
	 * @param AutomationOrderLine $automation_order_line
	 */
	public function setAutomationOrderLine( AutomationOrderLine $automation_order_line ): void{
		$this->automation_order_line = $automation_order_line;
	}





	/**
	 * @return int
	 */
	public function getSoldFromDepartmentId(): int{
		return $this->sold_from_department_id;
	}





	/**
	 * @param int $sold_from_department_id
	 */
	public function setSoldFromDepartmentId( int $sold_from_department_id ): void{
		$this->sold_from_department_id = $sold_from_department_id;
	}





	/**
	 * @return bool
	 */
	public function isMarkedAsSoldInGms(): bool{
		return $this->marked_as_sold_in_gms;
	}





	/**
	 * @param bool $marked_as_sold_in_gms
	 */
	public function setMarkedAsSoldInGms( bool $marked_as_sold_in_gms ): void{
		$this->marked_as_sold_in_gms = $marked_as_sold_in_gms;
	}





	/**
	 * @return int
	 */
	public function getQuantity(): int{
		return $this->quantity;
	}





	/**
	 * @param int $quantity
	 */
	public function setQuantity( int $quantity ): void{
		$this->quantity = $quantity;
	}





}
?>

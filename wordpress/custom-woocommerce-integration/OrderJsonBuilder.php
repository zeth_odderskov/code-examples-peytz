<?php




class OrderJsonBuilder {
	private AutomationOrder $automation_order;




	public function __construct( $automation_order ){
		$this->setAutomationOrder( $automation_order );
	}




	private function getPickupDepartmentUrl(): string {

		$pickup_department_url = '';
		if( $this->getShippingMethodId() === 'local_pickup_plus' ){
			$department_id = $this->getPickUpDepartmentId();
			$department = new Department( $department_id );
			$pickup_department_url = $department->getUrl();
		}
		return $pickup_department_url;
	}





	private function getPackageSlipLink(): string {
		return get_home_url() . '/wp-admin/admin-ajax.php?action=generate_wpo_wcpdf&document_type=packing-slip&order_ids='. $this->getOrderId();
	}






	private function getOrderLinesForJson(): array {

		// Order lines
		$order_lines = [];
		$initial_order_lines = $this->getAutomationOrderLines();
		foreach( $initial_order_lines as $order_line_id => $initial_order_line ){
			$wc_order_line = $initial_order_line->getOrderLine();
			$quantity = $wc_order_line->get_quantity();
			$product_name = $wc_order_line->get_name();
			$wc_product = wc_get_product( $wc_order_line->get_product_id() );
			$single_price = floatval( $wc_order_line->get_total() ) / $quantity;

			if( $wc_product ){

				// PRODUCT EXISTS
				$sku = $wc_product->get_sku();
				$product_id = $wc_product->get_id();
				$total_cost = @( floatval( $wc_order_line->get_meta( '_cost' ) ) ) ?: 0;
				$product_type = $initial_order_line->getCreatedFromGmsObject();
				// $price = @( floatval( $wc_order_line->get_total() ) ) ?: 0;

			} else {

				// FALLBACK
				// This should never happen. But it could if:
				//  - Product is deleted
				$sku = 1045;
				$product_id = 0;
				$total_cost = $wc_order_line->get_meta('_cost');
				$product_type = 'deleted';
				// $line_price = $wc_order_line->get_total();
			}

			$gms_info = [];
			foreach( $initial_order_line->getAutomationProducts() as $automation_product ){

				// Pur ID
				$line_price = 0;

				$pur_id = 0;
				if( is_a( $automation_product, 'AutomationPur' ) ){
					$pur_id = str_replace( 'PUR', '', $automation_product->getFullPurPartId() );
					$line_price = $single_price * $automation_product->getQuantity();
				}

				// Part ID
				$part_id = 0;
				if( is_a( $automation_product, 'AutomationPart' ) ){
					$parsed_full_part_id = GmsAccessory::parseFullPartId( $automation_product->getFullPurPartId() );
					$part_id = $parsed_full_part_id[ 'part_id' ];
					$quantity_sold = floatval( $parsed_full_part_id[ 'quantity' ] );
					$line_price = $single_price * $quantity_sold;
				}

				$location_id_on_time_of_being_sold = $automation_product->getSoldFromDepartmentId();
				$quantity = $automation_product->getQuantity();

				$gms_info[] = [
					'pur_id' => $pur_id,
					'part_id' => $part_id,
					'location_id_on_time_of_being_sold' => $location_id_on_time_of_being_sold,
					'quantity_sold_from_department' => $quantity,
					'price' => $line_price
				];
			}

			$order_lines[ $order_line_id ] = [
				'product_name' => $product_name,
				'SKU' => $sku,
				'product_id' => $product_id,
				'total_cost' => $total_cost,
				'product_type' => $product_type,
				'quantity' => $quantity,
				'gms_info' => $gms_info
			];
		}

		return $order_lines;
	}






	public function buildFullJson(): array {

		$returned = [];
		$returned[ 'order_id' ] = $this->getOrderId();
		$returned[ 'package_slip_link' ] = $this->getPackageSlipLink();
		$returned[ 'shipping_method' ] = $this->getShippingMethodId();
		$returned[ 'payment_method' ] = $this->getPaymentMethod();
		$returned[ 'pickup_department_url' ] = $this->getPickupDepartmentUrl();
		$returned[ 'final_internal_destination' ] = $this->getFinalInternalDestination();
		$returned[ 'order_details' ]= $this->getOrderDetailsJson();
		$returned[ 'order_lines' ] = $this->getOrderLinesForJson();
		return $returned;
	}





	private function getOrderDetailsJson(): array {
		$wc_order = $this->getWcOrder();

		$billing_first_name = @( $wc_order->get_billing_first_name() ) ?: '';
		$billing_last_name = @( $wc_order->get_billing_last_name() ) ?: '';
		$billing_company = @( $wc_order->get_billing_company() ) ?: '';
		$billing_address_1 = @( $wc_order->get_billing_address_1() ) ?: '';
		$billing_address_2 = @( $wc_order->get_billing_address_2() ) ?: '';
		$billing_city = @( $wc_order->get_billing_city() ) ?: '';
		$billing_state = @( $wc_order->get_billing_state() ) ?: '';
		$billing_postcode = @( $wc_order->get_billing_postcode() ) ?: '';
		$billing_country = @( $wc_order->get_billing_country() ) ?: '';
		$billing_email = @( $wc_order->get_billing_email() ) ?: '';
		$billing_phone = @( $wc_order->get_billing_phone() ) ?: '';
		$billing_phone = strval( $billing_phone );
		if( strlen( $billing_phone ) != 8 ){
			$billing_phone = '00000000';
		}

		$shipping_first_name = @( $wc_order->get_shipping_first_name() ) ?: '';
		$shipping_last_name = @( $wc_order->get_shipping_last_name() ) ?: '';
		$shipping_company = @( $wc_order->get_shipping_company() ) ?: '';
		$shipping_address_1 = @( $wc_order->get_shipping_address_1() ) ?: '';
		$shipping_address_2 = @( $wc_order->get_shipping_address_2() ) ?: '';
		$shipping_city = @( $wc_order->get_shipping_city() ) ?: '';
		$shipping_state = @( $wc_order->get_shipping_state() ) ?: '';
		$shipping_postcode = @( $wc_order->get_shipping_postcode() ) ?: '';
		$shipping_country = @( $wc_order->get_shipping_country() ) ?: '';
		$customer_note = @( $wc_order->get_customer_note() ) ?: '';

		$order_details = [
			'billing_info' => [
				'name' => $billing_first_name . ' ' . $billing_last_name,
				'address1' => $billing_address_1,
				'address2' => $billing_address_2,
				'zip' => $billing_postcode,
				'city' => $billing_city,
				'email' => $billing_email,
				'phone' => $billing_phone,
			],
			'shipping_info' => [
				'name' => $shipping_first_name . ' ' . $shipping_last_name,
				'address1' => $shipping_address_1,
				'address2' => $shipping_address_2,
				'zip' => $shipping_postcode,
				'city' => $shipping_city,
			],
			'customer_note' => $customer_note
		];

		return $order_details;
	}






	/**
	 * @return AutomationOrder
	 */
	public function getAutomationOrder(): AutomationOrder{
		return $this->automation_order;
	}





	/**
	 * @param AutomationOrder $automation_order
	 */
	public function setAutomationOrder( AutomationOrder $automation_order ): void{
		$this->automation_order = $automation_order;
	}





	private function getWcOrder(){
		return $this->automation_order->getOrder();
	}





	private function getPaymentMethod(): string {
		$wc_order = $this->getWcOrder();
		return $wc_order->get_payment_method();
	}





	private function getOrderId(){
		return $this->automation_order->getOrderId();
	}




	private function getShippingMethodId(){
		return $this->automation_order->getShippingMethodId();
	}




	private function getPickUpDepartmentId(){
		return $this->automation_order->getPickUpDepartmentId();
	}




	private function getFinalInternalDestination(): int {

		$final_internal_destination = $this->automation_order->getFinalInternalDestination();
		if( $final_internal_destination == 0 ){
			$final_internal_destination = 16;
		}

		return $final_internal_destination;
	}





	private function getAutomationOrderLines(): array {
		return $this->automation_order->getOrderLines();
	}




}
?>

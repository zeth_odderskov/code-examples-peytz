<?php





class AutomationPart extends AutomationProduct {
	private GmsAccessory $gms_accessory;





	public function __construct( $full_pur_part_id, GmsAccessory $gms_accessory, AutomationOrderLine $automation_order_line ){

		$validation_errors = GmsAccessory::validateFullPartId( $full_pur_part_id );
		if( !empty( $validation_errors) ){
			throw new Exception( '$full_part_id wasnt valid: ' . $full_pur_part_id . ' Validation errors: ' . implode( ', ', $validation_errors ) );
		}

		$parsed_part_id = GmsAccessory::parseFullPartId( $full_pur_part_id );
		$this->setQuantity( $parsed_part_id[ 'quantity' ] );

		$this->setGmsAccessory( $gms_accessory );
		parent::__construct( $full_pur_part_id, $automation_order_line );
	}




	public function getFullPartId(): string {
		return $this->getFullPurPartId();
	}





	public function increaseQuantity( int $quantity_to_add ){

		// Fix quantity
		$current_quantity = $this->getQuantity();
		$this->setQuantity( $quantity_to_add + $current_quantity );

		// Fix full_pur_part_id
		$full_pur_part_id = $this->getFullPurPartId();
		$exploded = explode( '#', $full_pur_part_id );
		$old_quantity = intval( $exploded[1] );
		$new_quantity = $old_quantity + $quantity_to_add;
		$new_full_pur_part_id = $exploded[0] . '#' . $new_quantity;
		$this->setFullPurPartId( $new_full_pur_part_id );
	}






	public function getCostPrice() {
		return $this->getGmsAccessory()->getCostPrice();
	}





	public function getWpId(){
		return $this->getGmsAccessory()->getWpId();
	}





	/**
	 * @return GmsAccessory
	 */
	public function getGmsAccessory(): GmsAccessory{
		return $this->gms_accessory;
	}





	/**
	 * @param GmsAccessory $gms_accessory
	 */
	public function setGmsAccessory( GmsAccessory $gms_accessory ): void{
		$this->gms_accessory = $gms_accessory;
	}





}
?>

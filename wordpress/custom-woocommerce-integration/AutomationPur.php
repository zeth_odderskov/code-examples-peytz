<?php





class AutomationPur extends AutomationProduct {
	private GmsPur $gms_pur;
	public array $order_line_ids_for_addons = [];





	public function __construct( $full_pur_part_id, GmsPur $gms_pur, AutomationOrderLine $automation_order_line, $quantity = 1 ){
		$this->setGmsPur( $gms_pur );
		parent::__construct( $full_pur_part_id, $automation_order_line );
		$this->setQuantity( $quantity );
	}




	public function pickRandomAddon( array $addon_gross_list ): array {
		$addon_product_ids = $this->getAddonProductIds();
		foreach( $addon_product_ids as $short_addon_product_id ){
			$addon_product_id = 'PART' . $short_addon_product_id;
			if( array_key_exists( $addon_product_id, $addon_gross_list ) ){
				$part = $addon_gross_list[ $addon_product_id ];
				$short_part_id = $part->getPartId();
				$part_id = 'PART' . $short_part_id;
				$part_stock = $part->getStock();
				arsort( $part_stock); // Sort for highest value first
				$dep_id = array_key_first(( $part_stock ) );
				$stock_qty = $part_stock[ $dep_id ];

				// If not stock
				if( $stock_qty == 0 ){
					$this->getAutomationOrder()->addStepValidationError( 'Stock is 0 for the part_id: ' . $part_id );
					continue;
				}

				if( $stock_qty > 0 ){
					$highest_stock_semi_full_part_id = $part_id . '_' . $dep_id;
					$addon_gross_list[ $part_id ]->reduceStockForDepartment( $dep_id, 1 );
					$automation_order = $this->getAutomationOrder();
					$order_line = $automation_order->getOrderLineFromPartId( $part_id );

					if( $order_line ){
						$highest_stock_full_part_id = $highest_stock_semi_full_part_id . '#1';
						$automation_order->addAdditionalAddonToExistingOrderLine( $order_line, $highest_stock_full_part_id, $this );
					} else {
						$automation_order->addAddonOrderLineAndAutomationProduct( $part_id, $this, $dep_id );
					}
				}
			}
		}

		return $addon_gross_list;
	}




	public function pickSpecificAddon( array $addon_gross_list, $dep_id ): array {
		$addon_product_ids = $this->getAddonProductIds();
		foreach( $addon_product_ids as $short_addon_product_id ){
			$addon_product_id = 'PART' . $short_addon_product_id;
			if( array_key_exists( $addon_product_id, $addon_gross_list ) ){
				$part = $addon_gross_list[ $addon_product_id ];
				$short_part_id = $part->getPartId();
				$part_id = 'PART' . $short_part_id;
				$part_stock = $part->getStock();
				$stock_qty = $part_stock[ $dep_id ];

				// If not stock
				if( $stock_qty == 0 ){
					$this->getAutomationOrder()->addStepValidationError( 'Best-case attempted to run, but apparently, there wasnt enough in stock anyways! Stock is 0 for the part_id: ' . $part_id );
					continue;
				}

				if( $stock_qty > 0 ){
					$semi_full_part_id_to_pick = $part_id . '_' . $dep_id;
					$addon_gross_list[ $part_id ]->reduceStockForDepartment( $dep_id, 1 );
					$automation_order = $this->getAutomationOrder();
					$order_line = $automation_order->getOrderLineFromPartId( $part_id );

					if( $order_line ){
						$full_part_id_to_pick = $semi_full_part_id_to_pick . '#1';
						$automation_order->addAdditionalAddonToExistingOrderLine( $order_line, $full_part_id_to_pick, $this );
					} else {
						$automation_order->addAddonOrderLineAndAutomationProduct( $part_id, $this, $dep_id );
					}
				}
			}

		}

		return $addon_gross_list;
	}





	/**
	 * Verifies that the order_line_id contains the PUR
	 * corresponding to this AutomationPur
	 */
	private function verifyPurRelationOnAddonOrderLine( $order_line_id ): bool {
		$order_line = $this->getAutomationOrderLine()->getAutomationOrder()->getOrderLineFromOrderLineId( $order_line_id );
		if( ! $order_line ){
			return false;
		}

		$parsed_meta_data = $order_line->getParsedMetaData();
		$full_pur_id = $this->getFullPurPartId();
		if( array_key_exists( '_addon-to', $parsed_meta_data ) ){
			$addon_to = $parsed_meta_data[ '_addon-to' ];
			$was_found = false;
			foreach( $addon_to as $entry ){
				if( str_contains( $entry, $full_pur_id ) ){
					$was_found = true;
				}
			}
			if( ! $was_found ){
				return false;
			}
		} else {
			return false;
		}

		return true;
	}





	public function isAddonsHandled(){

		/**
		 * DISREGARD CASES
		 */
		// Don't check this at all for parts
		if( $this->isPart() ){
			return true;
		}

		// Handled manually (break)
		$order_line = $this->getAutomationOrderLine();
		$parsed_meta_data = $order_line->getParsedMetaData();
		if( array_key_exists( '_addons-handled-manually', $parsed_meta_data ) ){
			return true;
		}

		// If no addons should be set (-)
		$required_addon_product_ids = $this->getAddonProductIds();
		if( count( $required_addon_product_ids ) === 1 && $required_addon_product_ids[0] == '-' ){
			return true;
		}
		// END OF DISREGARD CASES

		// Verify that there are as many order_line_ids as there are packet_part_ids to the GmsPur
		$packet_part_ids = $this->getGmsPur()->getPacketPartIds( 'array' );
		$order_line_ids_for_addons = $this->getOrderLineIdsForAddons();
		if( count( $packet_part_ids ) !== count( $order_line_ids_for_addons ) ){
			return false;
		}

		// If there is addon-order-line-ids,
		// ... but that the connection to this PUR isn't valid
		$order_line_ids_for_addons = $this->getOrderLineIdsForAddons();
		foreach( $order_line_ids_for_addons as $order_line_id ){
			$verified = $this->verifyPurRelationOnAddonOrderLine( $order_line_id );
			if( ! $verified ){
				return false;
			}
		}

		return true;
	}





	public function getWpId(){
		return $this->getGmsPur()->getWpId();
	}





	public function getImei(){
		return $this->getGmsPur()->getImei();
	}





	public function getGmsCategory(){
		return $this->getGmsPur()->getGmsCategory();
	}





	public function getLocationDepartmentId(){
		return $this->getGmsPur()->getLocationDepartmentId();
	}





	public function getCostTotalPrice(){
		return $this->getGmsPur()->getCostTotalPrice();
	}





	public function getModelTypeId(){
		return $this->getGmsPur()->getModelTypeId();
	}






	/**
	 * @return GmsPur
	 */
	public function getGmsPur(): GmsPur{
		return $this->gms_pur;
	}





	/**
	 * @param GmsPur $gms_pur
	 */
	public function setGmsPur( GmsPur $gms_pur ): void{
		$this->gms_pur = $gms_pur;
	}





	/**
	 * @return array
	 */
	public function getAddonProductIds(): array{
		return $this->getGmsPur()->getPacketPartIds( 'array' );
	}





	/**
	 * @return array
	 */
	public function getOrderLineIdsForAddons(): array{
		return $this->order_line_ids_for_addons;
	}





	/**
	 * @param int $order_line_id
	 */
	public function addOrderLineIdForAddons( int $order_line_id ): void{
		if( ! in_array( $order_line_id, $this->order_line_ids_for_addons ) ){
			$this->order_line_ids_for_addons[] = $order_line_id;
		}
	}





	/**
	 * @param array $order_line_ids_for_addons
	 */
	public function setOrderLineIdsForAddons( array $order_line_ids_for_addons ){
		$this->order_line_ids_for_addons = $order_line_ids_for_addons;
	}





}
?>

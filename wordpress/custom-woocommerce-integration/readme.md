# Custom WooCommerce Integration

**Kunde:** GreenMind  
**Live-eksempel:**  [https://greenmind.dk](https://greenmind.dk)

## Kort om opgaven

Når der blev solgt et produkt på denne WooCommerce-shop, så skal der automatisk udvælges og markeres, hvilke unikke enheder ordren omhandler. 

Det løste jeg ved at `AutomationOrder.php` er en wrapper rundt om en WooCommerce ordre (`wc_order`), som jeg så har lavet alle mulige ekstra funktioner til. 


## Hvad demonstrerer denne kode

Denne kode demonstrerer et større setup af custom PHP-classes - og hvordan jeg har sammensat en stort flow af operationer der skal sker efter hinanden.

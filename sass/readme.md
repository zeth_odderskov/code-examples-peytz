# SASS-struktur

**Kunde:** JV Consulting  
**Live-eksempel:**  [https://jvconsulting.dk/](https://jvconsulting.dk/)

## Kort om opgaven

Dette er et eksempel på, hvordan min SASS-struktur ser ud. Jeg følger [7-1 SASS-arkitekturen](https://www.learnhowtoprogram.com/user-interfaces/building-layouts-preprocessors/7-1-sass-architecture).

## Hvad demonstrerer denne kode

Dette er jo et produkt af alle mine år som front-end-udvikler.  
Så jeg ved nærmest ikke, hvor jeg skal starte her. :-)

Det der demonstrerer mest tankekraft vil jeg sige at måden jeg har opbygget mixins til at styre så meget som muligt. 
